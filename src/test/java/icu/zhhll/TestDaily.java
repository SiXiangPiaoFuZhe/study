package icu.zhhll;


import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

/**
 * @author zh
 * @date 2024/3/26 14:30
 */
public class TestDaily {

    @Test
    public void test(){
        Integer i = new Integer(100);
        Integer i2 = new Integer(100);
        // false
        System.out.println(i == i2);
        int i3 = 100;
        // true
        System.out.println(i == i3);
        Integer i4 = 100;
        // false
        System.out.println(i == i4);
    }

    @Test
    public void test02() throws UnsupportedEncodingException {
        String s = "\\x22isModify\\x22:false,\\x22id\\x22:22,\\x22folder_id\\x22:0,\\x22name\\x22:\\x22s\\xE6\\x8C\\x89\\xE6\\x97\\xB6\\xE9\\x97\\xB4\\x22,\\x22description\\x22:\\x22\\xE6\\x98\\xAF\\xE5\\xA4\\x9A\\xE5\\xB0\\x91\\xE5\\x9C\\xB0\\xE6\\x96\\xB9\\x22";
        String utf8 = URLDecoder.decode(s.replaceAll("\\\\x", "%"), "utf8");
        System.out.println(URLDecoder.decode(s.replaceAll("\\\\x", "%"), "utf8"));
        System.out.println(new String(utf8.getBytes(StandardCharsets.UTF_8.name())));
        System.out.println(new String(utf8.getBytes(StandardCharsets.ISO_8859_1.name())));
    }
}
