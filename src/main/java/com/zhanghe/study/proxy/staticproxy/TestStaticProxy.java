package com.zhanghe.study.proxy.staticproxy;

/**
 * 静态代理
 */
public class TestStaticProxy {
    public static void main(String[] args) {
        NikeClothFactory nikeClothFactory = new NikeClothFactory();
        ProxyFactory proxyFactory = new ProxyFactory(nikeClothFactory);
        proxyFactory.productCloth();
    }
}

// 接口
interface ClothFactory{
    void productCloth();
}

// 被代理类
class NikeClothFactory implements ClothFactory{

    @Override
    public void productCloth() {
        System.out.println("Nike工厂生产了一件Nike");
    }
}

// 代理类
class ProxyFactory implements ClothFactory{

    private ClothFactory clothFactory;

    public ProxyFactory(ClothFactory clothFactory){
        this.clothFactory = clothFactory;
    }
    @Override
    public void productCloth() {
        System.out.println("代理类开始执行，准备调用被代理类");
        clothFactory.productCloth();
    }
}
