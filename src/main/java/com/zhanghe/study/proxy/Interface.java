package com.zhanghe.study.proxy;

/**
 * @author: zhanghe
 * @date: 2020/5/27 17:16
 */
public interface Interface {
    void doSomething();

    void somethingElse(String arg);
}
