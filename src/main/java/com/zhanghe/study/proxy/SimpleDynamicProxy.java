package com.zhanghe.study.proxy;

import java.lang.reflect.Proxy;

/**
 * 代理设计模式的原理：使用一个代理将对象包装起来，然后用该代理对象取代原始对象，任何对原始对象
 * 的调用都要通过代理，代理对象决定是否以及何时调用原始对象上
 *
 * @author: zhanghe
 * @date: 2020/5/27 17:15
 */
public class SimpleDynamicProxy {
    public static void main(String[] args) {
        RealObject realObject = new RealObject();
        consumer(realObject);

        System.out.println("------------------");
        Interface inter = (Interface) Proxy.newProxyInstance(Interface.class.getClassLoader(), new Class[]{Interface.class}, new DynamicProxyHandler(realObject));
        consumer(inter);

    }

    public static void consumer(Interface inter) {
        inter.doSomething();
        inter.somethingElse("aa");
    }
}
