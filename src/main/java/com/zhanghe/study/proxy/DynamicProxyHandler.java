package com.zhanghe.study.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author: zhanghe
 * @date: 2020/5/27 17:12
 */
public class DynamicProxyHandler implements InvocationHandler {
    private Object proxy;
    public DynamicProxyHandler(Object proxy){
        this.proxy = proxy;
    }
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("proxy:"+proxy.getClass()+"method:"+method.getName());

        return method.invoke(this.proxy,args);
    }
}
