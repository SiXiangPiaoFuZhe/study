package com.zhanghe.study.proxy.cglib;

public class Test {

    public void print(){
        System.out.println("print方法执行");
        method();
    }

    public void method(){
        System.out.println("method方法执行");
    }

    public static void main(String[] args) {
        CglibProxy proxy = new CglibProxy();
        Test proxyImp = (Test) proxy.getProxy(Test.class);
        proxyImp.print();

    }
}
