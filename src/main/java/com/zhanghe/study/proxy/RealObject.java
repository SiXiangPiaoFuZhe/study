package com.zhanghe.study.proxy;

/**
 * @author: zhanghe
 * @date: 2020/5/27 17:17
 */
public class RealObject implements Interface {
    @Override
    public void doSomething() {
        System.out.println("RealObject doSomething");
    }

    @Override
    public void somethingElse(String arg) {
        System.out.println("RealObject somethingElse");
    }
}
