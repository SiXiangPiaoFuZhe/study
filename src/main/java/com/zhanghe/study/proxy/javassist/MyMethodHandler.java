package com.zhanghe.study.proxy.javassist;


import javassist.util.proxy.MethodHandler;

import java.lang.reflect.Method;

public class MyMethodHandler implements MethodHandler {
    @Override
    /**
     * self  生成的代理类
     * method  原始的方法
     * process  生成代理类的代理方法
     * objects 方法入参
     */
    public Object invoke(Object self, Method method, Method process, Object[] objects) throws Throwable {
        System.out.println("前置处理");
        Object obj = process.invoke(self,objects);
        System.out.println("后置处理");
        return obj;
    }
}
