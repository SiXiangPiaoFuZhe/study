package com.zhanghe.study.proxy.javassist;

import javassist.util.proxy.ProxyFactory;
import javassist.util.proxy.ProxyObject;


public class JavassistTest {

    public static ProxyFactory createProxy(){
        ProxyFactory factory = new ProxyFactory();
        factory.setSuperclass(JavassistMain.class);
        // 设置拦截器
        factory.setFilter(method -> {
            if("test".equals(method.getName())){
                return true;
            }
            return false;
        });


        return factory;
    }

    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        ProxyFactory factory = createProxy();
        Class clazz = factory.createClass();

        Object obj = clazz.newInstance();
        ((ProxyObject)obj).setHandler(new MyMethodHandler());
        ((JavassistMain)obj).test();

    }
}
