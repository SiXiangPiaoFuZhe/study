package com.zhanghe.study.ocr;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import java.io.File;
import java.io.IOException;

/**
 * @author zh
 * @date 2023/10/17 10:59
 */
public class Main {
    public static void main(String[] args) throws IOException, TesseractException {
        ITesseract tesseract = new Tesseract();
//        tesseract.setConfigs();
        tesseract.setDatapath("/usr/local/Cellar/tesseract/4.1.1/share/tessdata");
        tesseract.setLanguage("chi_sim");

//        BufferedImage logoImg = ImageIO.read(new File("/Users/zhanghe/Desktop/ka.jpeg"));
        String s = tesseract.doOCR(new File("/Users/zhanghe/Desktop/ka.jpeg"));

        System.out.println(s);
    }
}
