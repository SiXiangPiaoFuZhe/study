package com.zhanghe.study.net;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * InetAddress用来表示IP地址
 */
public class TestInetAddress {
    public static void main(String[] args) throws UnknownHostException {
        // 根据域名查找IP
        InetAddress inetAddress = InetAddress.getByName("www.baidu.com");
        System.out.println(inetAddress);
        System.out.println("域名："+inetAddress.getHostName());
        System.out.println("IP："+inetAddress.getHostAddress());

        // 获取本地地址
        InetAddress local = InetAddress.getLocalHost();
        System.out.println(local);
        System.out.println("域名："+local.getHostName());
        System.out.println("IP："+local.getHostAddress());
    }
}
