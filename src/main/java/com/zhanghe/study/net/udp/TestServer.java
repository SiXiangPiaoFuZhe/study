package com.zhanghe.study.net.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * UDP服务端
 */
public class TestServer {
    public static void main(String[] args) throws IOException {
        DatagramSocket socket = new DatagramSocket(9090);
        byte[] b = new byte[1024];
        DatagramPacket packet = new DatagramPacket(b,0,b.length);
        // 接收数据报
        socket.receive(packet);
        String str = new String(packet.getData(),0,packet.getLength());
        System.out.print(str);
        socket.close();
    }
}
