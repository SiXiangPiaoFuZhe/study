package com.zhanghe.study.net.udp;

import java.io.IOException;
import java.net.*;

/**
 * UDP客户端
 */
public class TestClient {
    public static void main(String[] args) throws IOException {
        DatagramSocket socket = new DatagramSocket();
        byte[] b = "我发送数据过去了".getBytes();
        // 数据报  每个数据报不能大于64K  每个数据报包含数据信息和IP、端口
        DatagramPacket packet = new DatagramPacket(b,0,b.length, InetAddress.getLocalHost(),9090);
        // 发送数据报
        socket.send(packet);
        socket.close();
    }
}
