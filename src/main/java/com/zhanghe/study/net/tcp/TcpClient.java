package com.zhanghe.study.net.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * TCP客户端
 */
public class TcpClient {
    public static void main(String[] args) {
        try (
                // 创建socket对象，指明服务端的IP和端口
                Socket socket = new Socket("127.0.0.1", 9090);
                // 发送数据
                OutputStream os = socket.getOutputStream();
                InputStream is = socket.getInputStream()) {

            os.write("客户端发来祝贺啦啦啦啦啦".getBytes());
            // 告诉服务端发送完了
            socket.shutdownOutput();
            byte[] b = new byte[20];
            int len;
            int count = 0;
            while((len = is.read(b)) != -1){
                count = count+len;
                String str = new String(b,0,len);
                System.out.print(str);
            }
            System.out.println(count);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        
    }
}
