package com.zhanghe.study.net.tcp;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author zh
 * @date 2022/5/4 09:41
 */

public class BioServer {
    public static void main(String[] args) {
        boolean flag = true;
        try {
            ServerSocket serverSocket = new ServerSocket(6666);
            Socket socket = serverSocket.accept();
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                writer.write(line + System.lineSeparator());
                // writer.flush();
            }
//            while (flag) {
//
//                if (socket != null) {
//
//                }
////                    new Thread(() -> {
////                        try {
////                            InputStream inputStream = socket.getInputStream();
////                            OutputStream outputStream = socket.getOutputStream();
////                            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream), 10);
////                            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream), 10);
////                            String line;
////                            while ((line = reader.readLine()) != null) {
////                                System.out.println(line);
////                                writer.write(line + System.lineSeparator());
////                                // writer.flush();
////                            }
////                        } catch (IOException e) {
////                            e.printStackTrace();
////                        }
////                    }).start();
////                }
//            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}





