package com.zhanghe.study.net.tcp;

import java.io.*;
import java.net.Socket;

/**
 * @author zh
 * @date 2022/5/4 09:41
 */
public class BioClient {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("127.0.0.1", 6666);
            OutputStream outputStream = socket.getOutputStream();
            InputStream inputStream = socket.getInputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream), 10);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String s = "hellowor\r\n.";
            System.out.println(s.length());
            writer.write(s);
            writer.flush();
//            socket.shutdownOutput();
            while (reader.readLine() != null) {
                String str = reader.readLine();
                System.out.println(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
