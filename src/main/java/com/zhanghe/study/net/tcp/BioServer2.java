package com.zhanghe.study.net.tcp;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 * @author zh
 * @date 2022/5/4 09:41
 */

public class BioServer2 {
    private static final Object obj = new Object();
    public static void main(String[] args) {
        boolean flag = true;
        try {
            ServerSocket serverSocket = new ServerSocket(6666);
            // 使用超时时间来设置为非阻塞状态,超过该时间会抛出SocketTimeoutException
            serverSocket.setSoTimeout(100);

            while (true){
                Socket socket = null;
                try{
                    // 设置了超时时间后accept就不会阻塞了
                    socket  = serverSocket.accept();
                } catch (SocketTimeoutException e){
                    synchronized (obj){   // 100ms内没有接收到任何数据，可以在这里做一些别的操作
                        System.out.println("没接收到数据，先歇一歇吧");
                        try {
                            obj.wait(10);
                        } catch (InterruptedException interruptedException) {
                            interruptedException.printStackTrace();
                        }
                    }
                    continue;

                }
                InputStream inputStream = socket.getInputStream();
                OutputStream outputStream = socket.getOutputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    System.out.println(line);
                    writer.write(line + System.lineSeparator());
                    // writer.flush();
                }
            }


//            while (flag) {
//
//                if (socket != null) {
//
//                }
////                    new Thread(() -> {
////                        try {
////                            InputStream inputStream = socket.getInputStream();
////                            OutputStream outputStream = socket.getOutputStream();
////                            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream), 10);
////                            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream), 10);
////                            String line;
////                            while ((line = reader.readLine()) != null) {
////                                System.out.println(line);
////                                writer.write(line + System.lineSeparator());
////                                // writer.flush();
////                            }
////                        } catch (IOException e) {
////                            e.printStackTrace();
////                        }
////                    }).start();
////                }
//            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}





