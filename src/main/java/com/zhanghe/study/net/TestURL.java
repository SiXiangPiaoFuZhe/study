package com.zhanghe.study.net;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

/**
 * URL
 */
public class TestURL {
    public static void main(String[] args) throws IOException {
        // 创建URL对象
        URL url = new URL("http://www.baidu.com");

        System.out.println(url.getProtocol());
        System.out.println(url.getHost());
        System.out.println(url.getPort());
        System.out.println(url.getFile());
        System.out.println(url.getRef());
        System.out.println(url.getQuery());
        // 读取资源
        InputStream is = url.openStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader reader = new BufferedReader(isr);
        String str;
        while((str = reader.readLine()) != null){
            System.out.println(str);
        }

        // 如果有输入有输出，使用URLConnection
        URLConnection connection = url.openConnection();
        InputStream cis = connection.getInputStream();
        InputStreamReader cisr = new InputStreamReader(cis);
        BufferedReader creader = new BufferedReader(cisr);

    }
}
