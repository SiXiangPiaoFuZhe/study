package com.zhanghe.study.huaweiod;

import java.util.*;

/**
 * @author: zhanghe
 * @date: 2020/7/19 22:20
 */
public class Main2 {
    public static void main(String[] args) {
        test(1,"AAAA");
//                Scanner scanner = new Scanner(System.in);
//        while (scanner.hasNext()) {
//            String b = scanner.next();
//            int a = scanner.nextInt();
//
//            test(a, b);
//        }
    }

    public static void test(int count,String s){
        Map<Character,Integer> map = new HashMap<>();
        char[] chars = s.toCharArray();
        int charCount = 0;
        char pre = chars[0];

        for(int i = 0;i<chars.length;i++){
            charCount ++;
            if(!map.containsKey(chars[i])) {
//                if(charCount != 0 && pre != chars[i]){
//                    pre = chars[i];
//                    map.put(chars[i-1],charCount);
//                    charCount = 0;
//                } else if(pre != chars[i]){
//                    pre = chars[i];
//                }

                if(pre != chars[i]){
                    charCount = 1;
                    pre = chars[i];
                }
                map.put(chars[i],charCount);
            } else {
                if(pre != chars[i]){
                    charCount = 1;
                    pre = chars[i];
                }
                if(map.get(chars[i]) < charCount){
                    map.put(chars[i],charCount);
                }
            }

        }
        List<Integer> list = new ArrayList<>();
        for (Map.Entry<Character,Integer> entry: map.entrySet()) {
            list.add(entry.getValue());
        }
        list.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if(o1 > o2){
                    return -1;
                } else if(o1 < o2){
                    return 1;
                }
                return 0;
            }
        });
        if(list.size() < count || count <= 0){
            System.out.println(-1);
        } else {
            System.out.println(list.get(count -1));
        }
    }
}
