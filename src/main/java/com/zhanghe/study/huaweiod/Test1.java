package com.zhanghe.study.huaweiod;

public class Test1 {
    public static void main(String[] args) {
        new Child("12");
    }
}

class People{
    public People(){
        System.out.print(1);
    }

    public People(String name){

        System.out.print(2);
    }
}

class Child extends People{

    public Child(){
        System.out.print(3);
    }

    public Child(String name){
        super("");
        System.out.print(4);
        new People(name);
    }

}
