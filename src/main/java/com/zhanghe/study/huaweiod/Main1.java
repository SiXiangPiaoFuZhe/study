package com.zhanghe.study.huaweiod;

/**
 * @author: zhanghe
 * @date: 2020/7/19 21:31
 */
public class Main1 {
    public static void main(String[] args) {
        // 97 -122 小写
        // 65 - 90 大写
//        Scanner scanner = new Scanner(System.in);
//        while (scanner.hasNext()) {
//            int a = scanner.nextInt();
//            String b = scanner.next();
//            test(a, b);
//        }

        test(1,
                "12abc-abCABc-4aB@");

    }

    public static void test(int count,String s){
        String[] ss = s.split("-");
        if(ss != null && ss.length >1){
            StringBuilder sb = new StringBuilder();
            for(int i = 1;i<ss.length;i++){
                sb.append(ss[i]);
            }
            if(count > 0){
                if(count == 1){
                    char[] chars = sb.toString().toCharArray();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(ss[0]).append("-");
                    for (int i = 0;i<chars.length;i++){
                        stringBuilder.append(chars[i]);
                        if(i != chars.length -1){
                            stringBuilder.append("-");
                        }
                    }
                    System.out.println(stringBuilder.toString());
                } else {
                    char[] chars = sb.toString().toCharArray();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(ss[0]).append("-");
                    StringBuilder temp = new StringBuilder();
                    // 记录小写数目
                    int xiaoxie = 0;
                    int daxie = 0;
                    for(int i = 0;i<chars.length;i++){

                        temp.append(chars[i]);
                        if(chars[i] >= 'a' && chars[i] <= 'z'){
                            xiaoxie++;
                        } else if(chars[i] >= 'A' && chars[i] <= 'Z'){
                            daxie++;
                        }
                        if(i != 0 && i != chars.length -1 && (i+1) % count == 0){
                            if(xiaoxie > daxie){
                                stringBuilder.append(temp.toString().toLowerCase());
                            } else if(daxie > xiaoxie){
                                stringBuilder.append(temp.toString().toUpperCase());
                            } else {
                                stringBuilder.append(temp);
                            }
                            xiaoxie = 0;
                            daxie = 0;
                            temp.delete(0,temp.length());
                            stringBuilder.append("-");
                        }
                    }
                    if(temp.length() >0){

                        if(xiaoxie > daxie){
                            stringBuilder.append(temp.toString().toLowerCase());
                        } else if(daxie > xiaoxie){
                            stringBuilder.append(temp.toString().toUpperCase());
                        } else {
                            stringBuilder.append(temp);
                        }
                    }
                    System.out.println(stringBuilder.toString());
                }
            }
        } else {
            System.out.println(s);
        }

    }
}
