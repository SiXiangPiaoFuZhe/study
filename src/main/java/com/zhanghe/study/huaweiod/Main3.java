package com.zhanghe.study.huaweiod;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

/**
 * @author: zhanghe
 * @date: 2020/7/19 23:20
 */
public class Main3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = new ArrayList<>();
        while (scanner.hasNext()) {

            int N = scanner.nextInt(); // 数组长度
            int x = scanner.nextInt(); // 所要比较的数字
            for(int i = 0;i<N;i++){
                list.add(scanner.nextInt());
            }
            test(x,list);
        }


    }

    public static void test(int x,List<Integer> list){
        list.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if(o1 > o2){
                    return -1;
                } else if(o1 < o2) {
                    return 1;
                }
                return 0;
            }
        });

        int sum = 0;
        for(int i = 0;i<list.size();i++){
            sum = sum + list.get(i);
        }
        // 第一种情况  全加上总数小于所要比较的值
        if(sum < x){
            System.out.println(0);
        } else {
            sum = 0;
            for(int i = 0;i<list.size();i++){
                sum = sum + list.get(i);
                if(sum > x){

                }
            }
        }
    }

}
