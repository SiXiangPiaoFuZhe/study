package com.zhanghe.study.inter;

/**
 * 接口和抽象类
 * @Author: zhanghe
 * @Date: 2020/5/8 11:08
 */
public class InterfImpl implements Interf {
    @Override
    public void execute() {
        System.out.println("开始执行");
    }

    public static void main(String[] args) {
        System.out.println(i);
        Interf interf = new InterfImpl();
        System.out.println(interf.geti());
        Interf.run(interf);
    }
}
