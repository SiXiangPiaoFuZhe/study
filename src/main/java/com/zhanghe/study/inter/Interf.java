package com.zhanghe.study.inter;

/**
 * 接口和抽象类
 * @Author: zhanghe
 * @Date: 2020/5/8 11:05
 */
public interface Interf {
    int i = 9;
    default int geti(){
        return i;
    }

    void execute();

    static void run(Interf interf){
        interf.execute();
    }
}
