package com.zhanghe.study.inter;

/**
 * 接口和抽象类
 * @Author: zhanghe
 * @Date: 2020/5/8 10:09
 */
public abstract class Basic {
    private int i = 0;

    abstract void func();

    protected abstract void func1();

    public abstract void func2();

    public void aa(){
        System.out.println(i);
    }

    public static void print(){
        System.out.println("输出");
    }

}
