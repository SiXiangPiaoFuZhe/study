package com.zhanghe.study.inter;

/**
 * 接口和抽象类
 * @Author: zhanghe
 * @Date: 2020/5/8 11:02
 */
public class BasicImpl extends Basic {
    @Override
    void func() {
        this.aa();
    }

    @Override
    public void func1() {
        print();
    }

    @Override
    public void func2() {
        System.out.println("func2");
    }

    public static void main(String[] args) {
        Basic basic = new BasicImpl();
        basic.func();
        basic.func1();
        basic.func2();
        basic.aa();
    }
}
