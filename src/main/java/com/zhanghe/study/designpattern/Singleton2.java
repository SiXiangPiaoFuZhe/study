package com.zhanghe.study.designpattern;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * 懒汉式单例 (双重锁)
 * @author: zhanghe
 * @date: 2020/7/20 14:25
 */
public class Singleton2 {

    private static volatile Singleton2 singleton = null;

    private Singleton2(){

    }

    public static Singleton2 getInstance(){
        if(singleton == null){
            synchronized (Singleton2.class){
                if(singleton == null){
                    singleton = new Singleton2();
                }
            }
        }
        return singleton;
    }

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<Singleton> cls = Singleton.class;
        Constructor<Singleton> constructor = cls.getDeclaredConstructor();
        constructor.setAccessible(true);
        Singleton singleton = constructor.newInstance();
        singleton.ss();
    }
}
