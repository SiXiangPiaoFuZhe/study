package com.zhanghe.study.designpattern;

/**
 * 懒汉式单例(线程不安全)
 *
 * @author: zhanghe
 * @date: 2020/7/20 14:23
 */
public class Singleton1 {
    private static Singleton1 singleton = null;

    private Singleton1() {

    }

    public static synchronized Singleton1 getInstance() {
        if (singleton == null) {
            singleton = new Singleton1();
        }
        return singleton;
    }
}
