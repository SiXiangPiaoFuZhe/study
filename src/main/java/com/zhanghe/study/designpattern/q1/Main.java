package com.zhanghe.study.designpattern.q1;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

/**
 * 夏天到了，各家各户的用电量都增加了许多，相应的电费也交的更多了。小玉家今天收到了一份电费通知单。小玉看到上面写：
 * 据闽价电[2006]27号规定，月用电量在150千瓦时及以下部分按每千瓦时0.4463元执行，
 * 月用电量在151~400千瓦时的部分按每千瓦时0.4663元执行，
 * 月用电量在401千瓦时及以上部分按每千瓦时0.5663元执行;
 * 小玉想自己验证一下，电费通知单上应交电费的数目到底是否正确呢。
 * 请编写一个程序，已知用电总计，根据电价规定，计算出应交的电费应该是多少。
 * @author zh
 * @date 2022/8/21 09:02
 */
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str_0 = scan.nextLine().trim();
        int amount = Integer.parseInt(str_0);

        scan.close();

        float result = solution(amount);

        System.out.println(result);

    }

    public static float solution(int amount){
        float result = (float)(0.0);

        // TODO: 请在此编写代码
        if(amount <= 150 && amount > 0){
            result = BigDecimal.valueOf(amount * 0.4463).divide(BigDecimal.ONE,1, RoundingMode.HALF_UP).floatValue();
        } else if(amount <= 400){
            result = BigDecimal.valueOf(150 * 0.4463 + (amount-150) * 0.4663).divide(BigDecimal.ONE,1, RoundingMode.HALF_UP).floatValue();
        } else if (amount > 400){
            result = BigDecimal.valueOf(150 * 0.4463 + (400-150) * 0.4663 + (amount-401) * 0.5663).divide(BigDecimal.ONE,1, RoundingMode.HALF_UP).floatValue();
        }

        return result;
    }
}
