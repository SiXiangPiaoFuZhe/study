package com.zhanghe.study.designpattern;

/**
 * 饿汉式单例
 *
 * @author: zhanghe
 * @date: 2020/7/20 14:21
 */
public class Singleton {
    private static Singleton singleton = new Singleton();

    private Singleton() {

    }

    public static Singleton getInstance() {
        return singleton;
    }

    public void ss(){
        System.out.println("aa");
    }
}
