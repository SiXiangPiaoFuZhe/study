package com.zhanghe.study.duotai;

/**
 * @author zh
 * @date 2021/6/13 15:42
 */
public interface Breathable {

    void breath();
}
