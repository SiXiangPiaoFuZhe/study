package com.zhanghe.study.duotai;

/**
 * @author zh
 * @date 2021/5/2 17:55
 */
public class TestExtendsField {

    public static void main(String[] args) {
        Parent p = new Child();
        // parent
        System.out.println(p.name);

        Child c = new Child();
        // child
        System.out.println(c.name);

    }


    static class Parent{
        public String name = "parent";
    }

    static class Child extends Parent{
        public String name = "child";
    }
}

