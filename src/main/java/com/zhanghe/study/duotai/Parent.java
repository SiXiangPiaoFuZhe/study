package com.zhanghe.study.duotai;

/**
 * 多态
 * @Author: zhanghe
 * @Date: 2020/5/8 0:24
 */
public class Parent implements Breathable{
    public int field = 2;

    public int getField(){
        return field;
    }

    private void testPrivate(){
        System.out.println("父类私有方法");
    }

    public Parent(int field){
        this.field = field;
    }

    public static void testStatic(){

    }

    public final void testFinal(){

    }

    public void amountField(){
        field+=5;
    }

    public Parent(){

    }

    @Override
    public void breath() {
        System.out.println("人当然会呼吸啦");
    }
}
