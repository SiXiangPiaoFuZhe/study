package com.zhanghe.study.duotai;

/**
 * 多态
 * @Author: zhanghe
 * @Date: 2020/5/8 0:21
 */
public class TestField {
    public static void main(String[] args) {
        Parent parent = new Child();
        System.out.println("parent.field= "+parent.field);
        System.out.println("parent.getField= "+parent.getField());
        Child child = new Child();
        System.out.println("child.field= "+child.field);
        System.out.println("child.getField= "+child.getField());
    }
}
