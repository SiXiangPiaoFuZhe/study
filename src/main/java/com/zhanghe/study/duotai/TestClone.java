package com.zhanghe.study.duotai;

/**
 * @author: zhanghe
 * @date: 2020/7/8 14:06
 */
public class TestClone implements Cloneable {
    private Person person;

    @Override
    protected Object clone() throws CloneNotSupportedException {
//        TestClone testClone = null;
//        testClone = (TestClone) super.clone();
//        testClone.person = (Person) person.clone();
//        return testClone;

        return super.clone();

    }

    @Override
    public String toString() {
        return "TestClone{" + super.toString() +
                "person=" + person + "{person.name }" + person.getName() +
                '}';
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        Person person = new Person();
        person.setName("张三");
        TestClone testClone = new TestClone();
        testClone.person = person;

        System.out.println(testClone);

        TestClone testClone1 = (TestClone) testClone.clone();
        System.out.println(testClone1);
        testClone1.person.setName("思思");
        System.out.println(testClone);
        System.out.println(testClone1);

    }
}
