package com.zhanghe.study.duotai;

/**
 * @author: zhanghe
 * @date: 2020/7/8 14:07
 */
public class Person implements Cloneable{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
