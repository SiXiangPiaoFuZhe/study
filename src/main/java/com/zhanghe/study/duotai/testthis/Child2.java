package com.zhanghe.study.duotai.testthis;

/**
 * @author zh
 * @date 2022/2/22 17:41
 */
public class Child2 extends Base{

    public void test(){
        System.out.println("Child#test执行");
    }


    public void invoke(Base base){
        base.method();
    }

    public static void main(String[] args) {
        Child2 child = new Child2();
        Base base = new Base();
        child.invoke(base);
    }
}
