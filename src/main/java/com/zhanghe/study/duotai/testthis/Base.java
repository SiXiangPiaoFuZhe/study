package com.zhanghe.study.duotai.testthis;

/**
 * @author zh
 * @date 2022/2/22 17:40
 */
public class Base {

    public void test(){
        System.out.println("Base#this执行");
    }

    public void method(){
        System.out.println("Base#method执行");
        this.test();
    }
}
