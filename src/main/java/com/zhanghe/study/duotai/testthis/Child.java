package com.zhanghe.study.duotai.testthis;

/**
 * @author zh
 * @date 2022/2/22 17:41
 */
public class Child extends Base{

    public void test(){
        System.out.println("Child#test执行");
    }


    public void invoke(){
        super.method();
    }

    public static void main(String[] args) {
        Child child = new Child();
        child.invoke();
    }
}
