package com.zhanghe.study.duotai;

public class Animal {
    private Integer type;
    private Integer legNums;

    private Animal(Integer type, Integer legNums) {
        this.type = type;
        this.legNums = legNums;
    }

    public Integer getType() {
        return type;
    }

    public Integer getLegNums() {
        return legNums;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Integer type;
        private Integer legNums;

        public Builder type(Integer type) {
            this.type = type;
            return this;
        }

        public Builder legNums(Integer legNums) {
            this.legNums = legNums;

            return this;
        }

        public Animal build() {
            return new Animal(type, legNums);
        }

        public enum AnimalEnum {
            DOG(1),

            CAT(2);

            private final int code;

            AnimalEnum(int code) {
                this.code = code;
            }

            public int getCode() {
                return code;
            }
        }
    }
    public static void main(String[] args) {
        Animal animal = Animal.builder().
                type(2).

                build();
        //下面这行等价于Integer 1egs = nu11;但是居然空指针了
        Integer legs = Builder.AnimalEnum.CAT.getCode() == animal.getType() ? animal.getLegNums() : 4;
        System.out.println("Legs: " + legs);
    }
}
