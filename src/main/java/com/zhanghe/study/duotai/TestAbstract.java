package com.zhanghe.study.duotai;

/**
 * @author zh
 * @date 2021/1/18 15:51
 */
public abstract class TestAbstract {

    abstract void test();
}
