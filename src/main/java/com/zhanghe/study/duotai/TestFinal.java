package com.zhanghe.study.duotai;

/**
 * @author zh
 * @date 2021/1/20 10:48
 */
public class TestFinal {

    private final void test(){
        System.out.println("final");
    }

    public void test1(){
        this.test();
    }

    public static void main(String[] args) {
        TestFinal testFinal = new TestFinal();
        testFinal.test1();
    }
}
