package com.zhanghe.study.duotai;

/**
 * 多态
 * @Author: zhanghe
 * @Date: 2020/5/8 0:24
 */
public class Child extends Parent {
    public int field = 4;


    @Override
    public int getField(){
        return field;
    }

    public void test(){

    }

    @Override
    public void amountField(){
        field+=5;
    }

    public static void main(String[] args) {
        Parent p = new Child();
        // 重写的实例方法
        p.amountField();
        // 调用静态方法
        p.testStatic();
        // 调用父类的final方法
        p.testFinal();

        Breathable breathable = new Child();
        // 实现的接口方法
        breathable.breath();
        System.out.println(p.getField());
        // 输出2，这里是直接拿的父类的字段值
        System.out.println(p.field);

    }



    // 静态方法不可被重写
//    public void testStatic(){
//
//    }
}
