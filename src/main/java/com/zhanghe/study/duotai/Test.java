package com.zhanghe.study.duotai;

/**
 * @author: zhanghe
 * @date: 2020/5/17 1:00
 */
public class Test {
    public Test value(Test t) {

        System.out.println("2" + t + "    " + t.getName());
        t.setName("老张");
        t = new Test();
        t.setName("小王");
        System.out.println("3" + t + "    " + t.getName());
        return t;
    }

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static void main(String[] args) {
        Test test = new Test();
        test.setName("小张");
        System.out.println("1" + test + "    " + test.getName());
        test.value(test);
        System.out.println("4" + test + "    " + test.getName());
    }
}
