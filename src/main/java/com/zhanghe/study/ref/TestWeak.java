package com.zhanghe.study.ref;

import java.lang.ref.WeakReference;
import java.util.Objects;

/**
 * 弱引用测试
 * @author zh
 * @date 2021/6/22 18:21
 */
public class TestWeak {
    public static void main(String[] args) {
        test1();
    }

    public static void test1(){
        TestRef obj = new TestRef();
        obj.setName("张三");
        // 创建弱引用，此时ref是通过弱引用指向的obj对象
        WeakReference<TestRef> ref = new WeakReference<>(obj);
        System.out.println(obj);
        // 清除强引用，如果不清除强引用的话，添加其他引用类型是没有作用的
        obj = null;
        System.gc();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // ref的get()方法可以获取到所引用的实际对象
        if(ref.get() != null){
            System.out.println(ref.get());
            System.out.println(Objects.requireNonNull(ref.get()).getName());
            // 清除引用
            ref.clear();
            System.out.println(ref.get());

        } else {
            System.out.println("弱引用已经被回收了");
        }

        // 输出结果
        // com.zhanghe.study.ref.TestRef@66d3c617
        //弱引用已经被回收了
    }
}
