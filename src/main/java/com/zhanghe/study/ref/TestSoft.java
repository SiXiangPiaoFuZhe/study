package com.zhanghe.study.ref;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.Objects;

/**
 * 软引用测试
 * @author zh
 * @date 2021/1/27 18:26
 */
public class TestSoft {
    public static void main(String[] args) {
        test1();
//        test2();
    }

    public static void test1(){
        TestRef obj = new TestRef();
        obj.setName("张三");
        // 创建软引用，此时ref是通过软引用指向的obj对象
        SoftReference<TestRef> ref = new SoftReference<>(obj);
        System.out.println(obj);
        // 清除强引用，如果不清除强引用的话，添加其他引用类型是没有作用的
        obj = null;
        System.gc();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // ref的get()方法可以获取到所引用的实际对象
        if(ref.get() != null){
            System.out.println(ref.get());
            System.out.println(Objects.requireNonNull(ref.get()).getName());
            // 清除引用
            ref.clear();
            System.out.println(ref.get());

        }

        // 输出结果
        //com.zhanghe.study.ref.TestRef@66d3c617
        //com.zhanghe.study.ref.TestRef@66d3c617
        //张三
        //null
    }

    public static void test2(){
        TestRef obj = new TestRef();
        obj.setName("张三");
        ReferenceQueue<TestRef> queue = new ReferenceQueue<>();
        // 创建软引用，此时ref是通过软引用指向的obj对象
        SoftReference<TestRef> ref = new SoftReference<>(obj,queue);
        System.out.println(obj);
        System.out.println(queue.poll());
        // 清除强引用，如果不清除强引用的话，添加其他引用类型是没有作用的
        obj = null;
        // ref的get()方法可以获取到所引用的实际对象
        if(ref.get() != null){
            System.out.println(ref.get());
            ref.enqueue();
            System.out.println(queue.poll());
            System.out.println(Objects.requireNonNull(ref.get()).getName());
            // 清除引用
            ref.clear();
            System.out.println(ref.get());
            System.out.println(queue.poll());

        }
    }


}
