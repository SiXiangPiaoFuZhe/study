package com.zhanghe.study.ref;

/**
 * @author zh
 * @date 2021/1/27 18:29
 */
public class TestRef {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
