package com.zhanghe.study.poi;

import java.io.IOException;
import java.util.List;

/**
 * @author zh
 * @date 2022/3/12 17:32
 */
public class Operate {

    public static void main(String[] args) throws IOException {
        List<Question> questions = ReadXls.readFromXls(0);

        WriteXls.writeToDoc(questions,"第三篇第十八章急诊科.docx");

        List<Question> questions2 = ReadXls.readFromXls(1);

        WriteXls.writeToDoc(questions2,"第三篇第十九章重症医学科.docx");
    }
}
