package com.zhanghe.study.poi;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * @author zh
 * @date 2022/3/12 17:31
 */
public class WriteXls {

    public static void writeToDoc(List<Question> questionList,String fileName) {

        //新建一个文档
        XWPFDocument doc = new XWPFDocument();

        boolean isFrist1 = true;
        boolean isFrist2 = true;
        boolean isFrist3 = true;

        int count1 = 1;
        int count2 = 1;
        int count3 = 1;

        for(Question question : questionList){
            //创建一个段落
            XWPFParagraph para = doc.createParagraph();
            if("A1型题".equals(question.getType())){ // 单选
                if(isFrist1){
                    //一个XWPFRun代表具有相同属性的一个区域
                    XWPFRun title = para.createRun();
                    title.setBold(true);//加粗
                    title.setText("单选题");
                    title.addBreak();
                    isFrist1 = false;
                }
                //一个XWPFRun代表具有相同属性的一个区域
                XWPFRun stem = para.createRun();
                stem.setBold(false);//加粗
                stem.setText(count1+".  "+question.getStem());
                stem.addBreak();

                for(char i = 'A'; i<='D'; i++){
                    XWPFRun option = para.createRun();
                    if(question.getAnswer().charAt(0) == i){
                        option.setColor("FF0000");
                    }
                    if(i == 'A'){
                        option.setText("A."+question.getOptionA());
                    } else if(i == 'B'){
                        option.setText("B."+question.getOptionB());
                    } else if(i == 'C'){
                        option.setText("C."+question.getOptionC());
                    } else if(i == 'D'){
                        option.setText("D."+question.getOptionD());
                    }
                    option.addTab();
                }

                count1++;
            } else if("X型题".equals(question.getType())){ // 多选
                if(isFrist2){
                    //一个XWPFRun代表具有相同属性的一个区域
                    XWPFRun title = para.createRun();
                    title.setBold(true);//加粗
                    title.setText("多选题");
                    title.addBreak();
                    isFrist2 = false;
                }
                //一个XWPFRun代表具有相同属性的一个区域
                XWPFRun stem = para.createRun();
                stem.setBold(false);//加粗
                stem.setText(count2+".  "+question.getStem());
                stem.addBreak();

                for(char i = 'A'; i<='E'; i++){
                    XWPFRun option = para.createRun();
                    char[] chars = question.getAnswer().toCharArray();

                    for(char aw : chars){
                        if(aw == i){
                            option.setColor("FF0000");
                        }
                    }

                    if(i == 'A'){
                        option.setText("A."+question.getOptionA());
                    } else if(i == 'B'){
                        option.setText("B."+question.getOptionB());
                    } else if(i == 'C'){
                        option.setText("C."+question.getOptionC());
                    } else if(i == 'D'){
                        option.setText("D."+question.getOptionD());
                    } else if(i == 'E'){
                        option.setText("E."+question.getOptionE());
                    }
                    option.addTab();
                }


                count2++;
            } else if("判断题".equals(question.getType())){ // 判断
                if(isFrist3){
                    //一个XWPFRun代表具有相同属性的一个区域
                    XWPFRun title = para.createRun();
                    title.setBold(true);//加粗
                    title.setText("判断题");
                    title.addBreak();
                    isFrist3 = false;
                }
                //一个XWPFRun代表具有相同属性的一个区域
                XWPFRun stem = para.createRun();
                stem.setBold(false);//加粗
                stem.setText(count3+".  "+question.getStem());
                stem.addBreak();

                for(char i = 'A'; i<='B'; i++){
                    XWPFRun option = para.createRun();

                    if(question.getAnswer().equals("正确") && i == 'A'){
                        option.setColor("FF0000");
                    } else if(question.getAnswer().equals("错误") && i == 'B'){
                        option.setColor("FF0000");
                    }

                    if(i == 'A'){
                        option.setText("A."+question.getOptionA());
                    } else if(i == 'B'){
                        option.setText("B."+question.getOptionB());
                    }
                    option.addTab();
                }

                count3++;
            }

        }
        OutputStream os;
        try {
            os = new FileOutputStream(fileName);
            //把doc输出到输出流
            doc.write(os);
            //关闭流
            doc.close();
            close(os);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void close(OutputStream os){
        if(os != null){
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
