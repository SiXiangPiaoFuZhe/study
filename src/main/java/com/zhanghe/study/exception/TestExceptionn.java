package com.zhanghe.study.exception;

/**
 * @author: zhanghe
 * @date: 2020/5/17 14:52
 */
public class TestExceptionn {
    public int f0(){
        int i =0;
        try{
            throw new RuntimeException();
        } catch (Exception e){
            i = 1;
            // 此时会记录下该值，不会立马返回，执行完finally之后在返回该值
            return i;
        } finally {
            i = 2;
        }
    }

    public int f(){
        int i =0;
        try{
            throw new RuntimeException();
        } catch (Exception e){
            i = 1;
            return i;
        } finally {
            i = 2;
            // 这里会返回最后一次的return
            return i;
        }
    }
    public static void main(String[] args) {
        TestExceptionn tst = new TestExceptionn();
        // 输出1
        System.out.println(tst.f0());
        // 输出2
        System.out.println(tst.f());
    }
}
