package com.zhanghe.study.exception;

/**
 * @author zh
 * @date 2024/2/4 17:24
 */
public class TestException02 {

    public static void main(String[] args) {
        // testEx catch
        //testEx finally
        //testEx1 finally
        testEx1();
    }

    public static boolean testEx1(){
        boolean ret = true;
        try{
            ret = testEx();
            if(!ret){
                return false;
            }
        } catch (Exception e){
            System.out.println("testEx1 catch");
            throw e;
        } finally {
            System.out.println("testEx1 finally");
            return ret;
        }
    }

    public static boolean testEx(){
        boolean ret = true;
        try{
            int i = 1/0;
        } catch (Exception e){
            System.out.println("testEx catch");
            ret = false;
            throw e;
        } finally {
            System.out.println("testEx finally");
            // finally return之后就不会抛出异常了，所以testEx1是正常返回的
            return ret;
        }
    }
}
