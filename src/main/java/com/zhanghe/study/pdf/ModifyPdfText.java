package com.zhanghe.study.pdf;

import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Canvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;

import java.io.File;
import java.io.IOException;

public class ModifyPdfText {  
  
    public static void main(String[] args) {

            String src = "/Users/zhanghe/Desktop/11.pdf";
            String dest = "modified.pdf";
            File file = new File(dest);
//            file.getParentFile().mkdirs();

            try (PdfReader reader = new PdfReader(src);
                 PdfWriter writer = new PdfWriter(dest);
                 PdfDocument pdfDoc = new PdfDocument(reader, writer)) {

                Document document = new Document(pdfDoc);

                // 遍历每一页
                for (int i = 1; i <= pdfDoc.getNumberOfPages(); i++) {
                    PdfPage page = pdfDoc.getPage(i);

                    // TODO: 这里需要实现逻辑来定位要修改的文本
                    // 例如，你可以搜索文本字符串，并找到它的位置。

                    // 假设我们已经知道了要替换的文本的位置和范围
                    Rectangle rect = new Rectangle(10, 20, 10, 30); // x, y, width, height 是文本的位置和大小

                    // 创建一个新的段落来替换旧文本
                    Paragraph newText = new Paragraph("新的文本内容");
                    newText.setTextAlignment(TextAlignment.LEFT); // 设置文本对齐方式

                    // 将新文本添加到文档中，覆盖旧文本的位置
                    Canvas canvas = new Canvas(pdfDoc.getFirstPage(), rect);
                    canvas.add(newText);

                    // 注意：这只是一个简单的示例，实际上可能需要更复杂的逻辑来确保文本正确替换，
                    // 特别是当文本跨越多行或多页，或者与页面上的其他元素重叠时。
                }

                document.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
}