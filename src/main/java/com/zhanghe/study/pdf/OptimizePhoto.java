package com.zhanghe.study.pdf;

import net.coobird.thumbnailator.Thumbnails;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author zh
 * @date 2023/6/21 22:48
 */
public class OptimizePhoto {

    public static void main(String[] args) {
        compressImage("/Users/zhanghe/Desktop/WechatIMG280.jpeg", 1080, 1092,null,
                "/Users/zhanghe/Desktop/fo.jpeg");


    }

    /**
     * compressImage
     *
     * @param path
     * @param width
     * @param height
     * @return
     */
    public static void compressImage(String path,
                                       int width, int height,
                                       String suffix,
                                       String outputFilename) {

        try {
//            ByteArrayOutputStream out = new ByteArrayOutputStream();
            BufferedImage image = Thumbnails.of(path)
//                    .size(width, height)
                    .scale(1)
                    .outputFormat(suffix)
                    .asBufferedImage();
            ImageIO.write(image,suffix, new File(outputFilename));

        } catch (IOException e) {


        }
    }
}
