package com.zhanghe.study.shujujiegou;

/**
 * 递归
 * @author zh
 * @date 2023/7/15 23:49
 */
public class DiGui {
    public static void main(String[] args) {
        System.out.println(function(5));
    }

    public static int function(int fact){
        if(fact <= 1){
            return 1;
        } else {
            return fact * function(fact - 1);
        }
    }
}
