package com.zhanghe.study.shujujiegou;

import java.util.Arrays;

/**
 * @author: zhanghe
 * @date: 2020/6/1 13:52
 */
public class ShunXuBiao<T> {
    // 默认容量
    private static final int DEFAULT_CAPACITY = 10;
    // 顺序表主体
    private Object[] array;
    // 顺序表长度，表示顺序表中当前元素个数
    private int size;

    //数组长度
    private int length;

    public static void main(String[] args) {
        ShunXuBiao<Integer> shunXuBiao = new ShunXuBiao<>(2);
        System.out.println(shunXuBiao.isEmpty());
        shunXuBiao.add(100);
        System.out.println(shunXuBiao.size());
        shunXuBiao.add(10);
        shunXuBiao.print();
        shunXuBiao.add(0, 20);
        shunXuBiao.print();
        shunXuBiao.remove(1);
        shunXuBiao.print();
        shunXuBiao.clearAll();
        shunXuBiao.add(30);
        shunXuBiao.print();
    }

    public void print() {
        System.out.println("-------------------");
        for (int i = 0; i < size; i++) {
            System.out.printf("%s\t", array[i]);
        }
        System.out.println();
        System.out.println("-------------------");
    }

    /**
     * 定义容量
     *
     * @param capacity
     */
    public ShunXuBiao(int capacity) {
        this.array = new Object[capacity];
        this.size = 0;
        this.length = capacity;
    }

    public ShunXuBiao() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * 清空顺序表
     */
    public void clearAll() {
        this.size = 0;
    }

    /**
     * 顺序表元素个数
     *
     * @return
     */
    public int size() {
        return this.size;
    }

    /**
     * 添加元素
     *
     * @param element
     */
    public void add(T element) {
        ensureCapacity();
        if (size < array.length) {
            array[size] = element;
            size++;
        } else {
            throw new RuntimeException("已超过数组容量");
        }
    }

    /**
     * 指定位置添加元素
     *
     * @param index
     * @param element
     */
    public void add(int index, T element) {
        ensureCapacity();
        if (index < size && size < array.length) {
//            Object[] oldArray = Arrays.copyOf(array, array.length);
//            array[index] = element;
//            for (int i = index; i < size; i++) {
//                array[i + 1] = oldArray[i];
//            }
            for (int i = size - 1; i >= index; i--) {
                array[i + 1] = array[i];
            }
            array[index] = element;
            size++;

        } else {
            throw new RuntimeException("数组下标越界");
        }
    }

    /**
     * 自动扩容
     */
    public void ensureCapacity() {
        if (size >= length) {
            // 容量增加一倍
            length = length << 1;
            array = Arrays.copyOf(array, length);
        }
    }

    /**
     * 删除指定元素
     *
     * @param index
     */
    public void remove(int index) {
        if (index < size) {
//            Object[] oldArray = Arrays.copyOf(array, array.length);
//            for (int i = index; i < size; i++) {
//                array[i] = oldArray[i + 1];
//            }
            for (int i = index; i < size-1; i++) {
                array[i] = array[i+1];
            }
            array[size - 1] = null;
            size--;
        }
    }


    /**
     * 获取数据
     *
     * @param index
     * @return
     */
    public T get(int index) {
        if (index < size) {
            return (T) array[index];
        }
        throw new RuntimeException("数组越界");
    }

    /**
     * 判断是否为空
     *
     * @return
     */
    public boolean isEmpty() {
        return size == 0;
    }
}
