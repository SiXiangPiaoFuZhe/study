package com.zhanghe.study.shujujiegou;

/**
 * @author: zhanghe
 * @date: 2020/6/2 10:02
 */
public class LianBiao<E> {

    public static void main(String[] args) {
        LianBiao<Integer> lianBiao = new LianBiao<>();
        lianBiao.addElement(10);
        lianBiao.addElement(20);
        lianBiao.print();
        lianBiao.deleteElement(0);
        lianBiao.print();
        lianBiao.addElement(30);
        lianBiao.print();
        lianBiao.addElement(40);
        lianBiao.deleteElement(1);
        lianBiao.print();
        System.out.println(lianBiao.getElement(0));

    }


    Node<E> head = null;//头节点
    private int length = 0;

    /**
     * 添加元素
     *
     * @param element
     */
    public void addElement(E element) {
        Node<E> newNode = new Node<>(element);
        if (head == null) { // 如果是空链表，添加到头节点
            head = newNode;
        } else {
            Node<E> temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = newNode;
        }
        length++;
    }

    /**
     * 删除第index个节点
     *
     * @param index
     */
    public void deleteElement(int index) {
        if (index < 0 || index >= length) {
            throw new RuntimeException("数组越界，无法删除");
        }
        // 去除原本头节点  头节点指向下一节点
        if (index == 0) {
            head = head.next;
            length--;
        } else {
            int count = 1;
            Node<E> preNode = head;
            Node<E> curNode = preNode.next;
            while (curNode != null) {
                if (index == count) {
                    curNode = curNode.next;
                    preNode.next = curNode;
                    length--;
                    break;
                } else {
                    preNode = curNode;
                    curNode = curNode.next;
                    count++;
                }
            }
        }
    }

    /**
     * 获取第index个节点数据
     *
     * @param index
     * @return
     */
    public E getElement(int index) {
        if (index < 0 || index > length) {
            throw new RuntimeException("数组越界，无法获取");
        }
        if (index == 0) {
            return head.data;
        } else {
            Node<E> curNode = head;
            int count = 0;
            while (curNode != null) {
                if (count == index) {
                    return curNode.data;
                }
                curNode = curNode.next;
                count++;
            }
        }
        return null;
    }

    public void print() {
        System.out.println("------------------------");
        Node<E> temp = head;
        while (temp != null) {
            System.out.printf("%s\t", temp.data);
            temp = temp.next;
        }
        System.out.println();
        System.out.println("------------------------");
    }


    private static class Node<E> {
        E data;
        Node<E> next;

        public Node(E data) {
            this.data = data;
        }
    }
}
