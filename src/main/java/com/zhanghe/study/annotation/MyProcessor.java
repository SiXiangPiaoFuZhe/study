package com.zhanghe.study.annotation;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.util.Set;

/**
 * @author: zhanghe
 * @date: 2020/6/8 19:22
 */
// 指定可以处理的注解类型
@SupportedAnnotationTypes({"com.zhanghe.study.annotation.*"})
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class MyProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for(TypeElement annotation : annotations){
            System.out.println(annotation);
        }
        for(Element element : roundEnv.getElementsAnnotatedWith(Test.class)){
            System.out.println(element.getKind() +
                    " : " + element.getModifiers() +
                    " : " + element.getSimpleName() +
                    " : " + element.asType());
        }



        return false;
    }
}
