package com.zhanghe.study.annotation;

/**
 * @author: zhanghe
 * @date: 2020/6/9 9:19
 */
@Test(name = "test_source")
public class TestSource {
    private String sex;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public static void main(String[] args) {

    }
}
