package com.zhanghe.study.inner;

/**
 * @Author: zhanghe
 * @Date: 2020/5/10 23:31
 */
public class MethodOuter {

    public Destination desc(String s){
        String name = "张三";
        // 局部内部类
        class Inner implements Destination{
            @Override
            public void f(){
                System.out.println("名称为"+name);
                System.out.println("Inner.f()传入参数"+s);
            }
        }
        return new Inner();
    }

    public static void main(String[] args) {
        MethodOuter methodOuter = new MethodOuter();
        methodOuter.desc("test").f();
    }
}
