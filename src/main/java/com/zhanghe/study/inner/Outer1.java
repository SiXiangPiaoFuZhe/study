package com.zhanghe.study.inner;

/**
 * @Author: zhanghe
 * @Date: 2020/5/10 23:55
 */
public class Outer1 {
    public Contents contents(){
        return new Contents(){
            @Override
            public void f() {
                System.out.println("Contents匿名内部类.f()");
            }
        };
    }

    public static void main(String[] args) {
        Outer1 outer1 = new Outer1();
        outer1.contents().f();
    }
}
