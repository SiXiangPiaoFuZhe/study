package com.zhanghe.study.inner;

/**
 * @Author: zhanghe
 * @Date: 2020/5/11 0:19
 */
public class Outer2 {
    private static class Inner2{
        private static int i = 0;
        private static void f(){
            System.out.println("Inner2.f()");
        }
    }

    public static void main(String[] args) {
        Inner2.f();
    }
}
