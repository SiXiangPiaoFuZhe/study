package com.zhanghe.study.inner;

/**
 * @Author: zhanghe
 * @Date: 2020/5/10 19:28
 */
public class DoThis {
    class Inner{
        public DoThis outer(){
            return DoThis.this;
        }
    }

    public void f(){
        System.out.println("DoThis.f()");
    }

    public Inner inner(){
        return new Inner();
    }

    public static void main(String[] args) {
        DoThis doThis = new DoThis();
        Inner inner = doThis.inner();
        inner.outer().f();

    }
}
