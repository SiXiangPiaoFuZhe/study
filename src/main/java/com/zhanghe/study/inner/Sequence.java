package com.zhanghe.study.inner;

/**
 * @Author: zhanghe
 * @Date: 2020/5/10 19:06
 */
public class Sequence {

    private Object[] items;

    private int next;

    public Sequence(int size) {
        items = new Object[size];
    }

    /**
     * 添加元素
     *
     * @param obj
     */
    public void add(Object obj) {
        if (next < items.length) {
            items[next] = obj;
            next++;
        }
    }

    public SequenceSelector selector() {
        return new SequenceSelector();
    }

    public static void main(String[] args) {
        Sequence sequence = new Sequence(10);
        for (int i = 0; i < 10; i++) {
            sequence.add(i);
        }
        SequenceSelector selector = sequence.selector();
        while (!selector.end()) {

            System.out.print(selector.current() + " ");
            selector.next();
        }

    }

    private class SequenceSelector {

        private int current = 0;

        public boolean end() {
            return current == size();
        }

        public int size() {
            return items.length;
        }

        public Object current() {
            return items[current];
        }

        public void previous() {
            if (current > 0) {
                current--;
            }
        }

        public void next() {
            if (current < items.length) {
                current++;
            }
        }

    }
}
