package com.zhanghe.study.inner;

/**
 * @Author: zhanghe
 * @Date: 2020/5/10 23:34
 */
public interface Destination {
    void f();
}
