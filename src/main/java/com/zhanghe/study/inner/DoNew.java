package com.zhanghe.study.inner;

/**
 * @Author: zhanghe
 * @Date: 2020/5/10 19:33
 */
public class DoNew {

     class Inner {
        public void f(){
            System.out.println("Inner.f()");
        }
    }

    public static void main(String[] args) {
        DoNew doNew = new DoNew();
        Inner inner = doNew.new Inner();
        inner.f();
    }
}
