package com.zhanghe.study.inner;

/**
 * @Author: zhanghe
 * @Date: 2020/5/10 18:51
 */
public class Outter {

    class Inner {
        private int[] ints = new int[100000];
        public void print(String s) {
            System.out.println(s);
        }
    }

    public Inner buildInner() {
        return new Inner();
    }

    public static void main(String[] args) {
        Outter outter = new Outter();
        for(int i = 0;i<100;i++){
            Inner inner = outter.buildInner();
            inner.print("输出");
        }

    }
}
