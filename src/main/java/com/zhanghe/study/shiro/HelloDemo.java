package com.zhanghe.study.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.env.BasicIniEnvironment;
import org.apache.shiro.subject.Subject;

import java.util.Arrays;

/**
 * @author zh
 * @date 2022/7/26 14:57
 */
public class HelloDemo {
    public static void main(String[] args) {
        Subject subject = login();

        System.out.println("是否为role1角色："+subject.hasRole("role1"));
        System.out.println("是否同时为role1和role2角色："+subject.hasAllRoles(Arrays.asList("role1","role2")));


        System.out.println("是否拥有update权限："+subject.isPermitted("user:update"));
    }

    public static Subject login(){
        // 环境准备
        BasicIniEnvironment environment = new BasicIniEnvironment("classpath:shiro.ini");
        // 全局配置，绑定SecurityManager
        SecurityUtils.setSecurityManager(environment.getSecurityManager());
        // 通过SecurityUtils获取subject，会自动绑定到当前线程
        Subject subject = SecurityUtils.getSubject();
        // 用户名密码验证token
        UsernamePasswordToken token = new UsernamePasswordToken("zhangsan", "123456");

        // 登录
        subject.login(token);

        System.out.println(subject.isAuthenticated());

        return subject;
    }
}
