package com.zhanghe.study.qu;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringJoiner;

/**
 * @author zh
 * @date 2022/5/31 07:38
 */
public class Test {
    public static void main(String[] args) {
        List<Fruit> fruitList = new ArrayList<>();
        init(fruitList);
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入用户名：");
        String name = scanner.next();
        System.out.println("请输入密码：");
        String password = scanner.next();

        if (name.equals("admin") && password.equals("123456")) {
            System.out.println("登录成功！");
            System.out.println("商品如下");
            for(Fruit fruit : fruitList){
                System.out.println(fruit);
            }
        } else {
            System.out.println("登录失败，请注册或者检查用户名和密码是否正确");
        }
    }

    private static void init(List<Fruit> fruitList){
        Fruit apple = new Fruit();
        apple.name = "苹果";
        apple.price = 2;
        apple.color = "红色";
        fruitList.add(apple);
        Fruit banana = new Fruit();
        banana.name = "香蕉";
        banana.price = 1;
        banana.color = "黄色";
        fruitList.add(banana);
        Fruit orange = new Fruit();
        orange.name = "橘子";
        orange.price = 1;
        orange.color = "橙色";
        fruitList.add(orange);
        Fruit watermelon = new Fruit();
        watermelon.name = "西瓜";
        watermelon.price = 1;
        watermelon.color = "绿色";
        fruitList.add(watermelon);
        Fruit cucumber = new Fruit();
        cucumber.name = "黄瓜";
        cucumber.price = 1;
        cucumber.color = "黄色";
        fruitList.add(cucumber);
    }


    static class Fruit {
        public String name;
        public String color;
        public Integer price;



        @Override
        public String toString() {
            return new StringJoiner(", ")
                    .add("name='" + name + "'")
                    .add("color='" + color + "'")
                    .add("price=" + price)
                    .toString();
        }
    }
}



