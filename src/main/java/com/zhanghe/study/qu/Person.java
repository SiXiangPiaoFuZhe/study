package com.zhanghe.study.qu;

/**
 * @author zh
 * @date 2022/5/8 10:13
 */
public class Person {

    private String name;

    private int age;

    public Person(String name,int age){
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
