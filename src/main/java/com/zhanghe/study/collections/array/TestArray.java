package com.zhanghe.study.collections.array;

/**
 * @author zh
 * @date 2021/1/18 11:51
 */
public class TestArray {
    public static void main(String[] args) {
        int[] numbers = {1,2,3};
        numbers = new int[]{1,2,3};

        int[][] numberss = {{1,2,3},{1,2,3}};
        System.out.println(numberss.length);
        for (int[] a : numberss){
            System.out.println(a);
            System.out.println(a.length);
        }
        System.out.println("================");
        numberss = new int[2][3];
        System.out.println(numberss.length);
        for (int[] a : numberss){
            System.out.println(a);
            for (int b : a){
                System.out.print(b+"\t");
            }
            System.out.println();
            System.out.println(a.length);
        }

    }
}
