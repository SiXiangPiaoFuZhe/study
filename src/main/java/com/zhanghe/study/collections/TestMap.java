package com.zhanghe.study.collections;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: zhanghe
 * @date: 2020/6/13 20:29
 */
public class TestMap {
    public static void main(String[] args) {
        Set set = new LinkedHashSet();
        Test test = new Test();
//        Map<Test,String> map = new HashMap<>();
//        Collections.synchronizedMap(map);
        Map<Test,String> map = new ConcurrentHashMap<>();
        map.put(test,"1");
        Test test1 = new Test();
        map.put(test1,"2");
        map.entrySet().stream().forEach(
                testStringEntry -> {
                    System.out.println(testStringEntry.getKey());
                    System.out.println(testStringEntry.getValue());
                }
        );
        System.out.println(map.size());

        Map map1 = new TreeMap();
    }

    static class Test{
        @Override
        public int hashCode() {
            return 1;
        }
    }
}
