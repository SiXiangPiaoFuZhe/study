package com.zhanghe.study.collections;

import java.util.PriorityQueue;

/**
 * @author zh
 * @date 2021/1/11 10:32
 */
public class TestPriorityQueue {
    public static void main(String[] args) {
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        queue.add(50);
        queue.add(20);
        queue.add(80);
        queue.add(120);
        queue.add(119);
        queue.add(480);
        queue.add(280);
        queue.add(89);
        queue.add(98);
        queue.add(125);
        queue.add(200);
        queue.add(450);
        queue.remove((Integer) 119);
    }
}
