package com.zhanghe.study.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author: zhanghe
 * @date: 2020/6/13 21:56
 */
public class TestCollections {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();

        list.add("qw");
        list.add("qa");
        list.add("qz");

        Collection<String> list1 = Collections.unmodifiableCollection(list);
        Collections.unmodifiableList(list);
        Collections.synchronizedCollection(list);
        list1.add("aaa");
    }
}
