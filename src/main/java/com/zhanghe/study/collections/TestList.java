package com.zhanghe.study.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * 集合
 *
 * @author: zhanghe
 * @date: 2020/5/11 10:56
 */
public class TestList {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>(1);

//        list.add("1");
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);
        list.add(null);

        System.out.println(list.size());
        Vector vector = new Vector();
//        list.add("J");
//        list.add("A");
//        list.add("V");
//        list.add("A");
//        for (int i = list.size() - 1; i >= 0; i--) {
//            String s = list.remove(i);
//            System.out.println(s);
//        }


        List<Integer> integers = new ArrayList<>();
        for(int i = 0;i<1000;i++){
            integers.add(i);
        }
        long start = System.currentTimeMillis();
        for(int i = 0,length = integers.size();i<length;i++){

        }
        long end = System.currentTimeMillis();
        System.out.println("耗时"+(end-start));

        start = System.currentTimeMillis();
        for(int i: integers){

        }
        end = System.currentTimeMillis();
        System.out.println("耗时"+(end-start));
    }
}
