package com.zhanghe.study.webservice;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * @author zh
 * @date 2021/11/7 16:42
 */
@WebService
public class TestWebService {

    @WebMethod
    public String sayHello(){
        return "hello";
    }
}
