package com.zhanghe.study.stream;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: zhanghe
 * @date: 2020/5/17 0:53
 */
public class TestStream {
    public void show(){
        System.out.println("show");
    }
    public static void main(String[] args) {
        List<TestStream> testStreamList = new ArrayList<>();
        testStreamList.stream().map(
                s -> new TestStream()
        );
    }
}
