package com.zhanghe.study.reflect;

import java.lang.reflect.Constructor;

/**
 * 测试构造器
 * // 使用newInstance方法要求该类必须有空参构造器，且权限修饰符要足够
 */
public class TestConstructor {
    public static void main(String[] args) throws NoSuchMethodException {
        Class clazz = TestConstructor.class;
        Constructor[] constructors = clazz.getConstructors();
        System.out.println("=========clazz.getConstructors()====只能获取到公有的构造器");
        for(Constructor constructor : constructors){
            System.out.println(constructor);
        }
        System.out.println("*****************");

        System.out.println("=========clazz.getConstructor()=====获取公有无参构造器");
        Constructor constructor = clazz.getConstructor();
        System.out.println(constructor);
        System.out.println("*****************");

        System.out.println("=========clazz.getDeclaredConstructors()=====获取所有构造器(包括私有、受保护、默认、公有)");
        Constructor[] declaredConstructors = clazz.getDeclaredConstructors();
        for(Constructor declaredConstructor : declaredConstructors){
            System.out.println(declaredConstructor);
        }
        System.out.println("*****************");

        System.out.println("=========clazz.getDeclaredConstructor()====获取指定参数类型的构造器");
        Constructor declaredConstructor = clazz.getDeclaredConstructor(String.class);
        System.out.println(declaredConstructor);
        System.out.println("*****************");

    }

    private TestConstructor(String name){

    }

    TestConstructor(int age){

    }

    protected TestConstructor(String[] args){

    }

    public TestConstructor(){

    }
}
