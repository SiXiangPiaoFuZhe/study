package com.zhanghe.study.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestType {

    static Map<String,String> map = new HashMap<>();

    static {
        map.put("11","22");
    }

    static Map.Entry<String,String> entry = map.entrySet().stream().findFirst().get();

    static List<String>[] list = null;



    public static void main(String[] args) throws NoSuchFieldException {

        // ParameterizedType 的getOwnerType方法
        Field field = TestType.class.getDeclaredField("entry");
        Type type = field.getGenericType();
        if(type instanceof ParameterizedType){
           Type type1 =  ((ParameterizedType) type).getOwnerType();
           System.out.println(type1);  // interface java.util.Map
        }

        System.out.println("================");

        // GenericArrayType 的getGenericComponentType方法
        Field field1 = TestType.class.getDeclaredField("list");
        Type arrayType = field1.getGenericType();
        if(arrayType instanceof GenericArrayType){
            Type arrayType1 =  ((GenericArrayType) arrayType).getGenericComponentType();
            System.out.println(arrayType1);  // java.util.List<java.lang.String>
        }


    }



}
