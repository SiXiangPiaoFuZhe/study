package com.zhanghe.study.reflect;

import java.lang.reflect.Method;

public class TestMethod {
    public static void main(String[] args) {
        // getDeclaredMethods可以获取到该类的所有方法  包括私有方法
        Method[] methods = TestMethod.class.getDeclaredMethods();
        System.out.println("=========getDeclaredMethods=============");
        for (Method method : methods){
            System.out.println(method.getName());
        }

        // getMethods可以获取到包含父类的方法  只能获取到public方法
        Method[] methods1 = TestMethod.class.getMethods();
        System.out.println("=========getMethods=============");
        for (Method method : methods1){
            System.out.println(method.getName());
        }

    }

    private void testPrivate(){

    }

    public void testPublic(){

    }

    void testDefault(){

    }

    protected void testProtected(){

    }
}
