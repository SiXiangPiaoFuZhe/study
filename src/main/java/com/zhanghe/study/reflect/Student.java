package com.zhanghe.study.reflect;

import java.util.List;

@MyAnnotation(name = "123")
public class Student<T> extends Person<T> implements Aliveable{

    private int grade;
    private List<Course> courseList;

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }

    @Override
    public void breathe() {
        System.out.println("我还可以深呼吸");
    }
}
