package com.zhanghe.study.reflect;

import java.io.Serializable;

public class Person<T> implements Serializable {

    private int age;
    private String name;
    private T cardId;

    public Person(){

    }

    public Person(T cardId){
        this.cardId = cardId;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public T getCardId() {
        return cardId;
    }

    public void setCardId(T cardId) {
        this.cardId = cardId;
    }

    public void display(){
        System.out.println("名字"+name+",年龄"+age+",身份证"+cardId);
    }
}
