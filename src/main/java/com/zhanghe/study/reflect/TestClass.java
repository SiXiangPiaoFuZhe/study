package com.zhanghe.study.reflect;

/**
 * 获取Class实例的方法
 *
 */
public class TestClass {
    public static void main(String[] args) throws ClassNotFoundException {
        // 第一种
        Class<Person> clazz = Person.class;

        // 第二种
        Person person = new Person();
        Class clazz1 =  person.getClass();

        // 第三种
        String className = "com.zhanghe.study.reflect.Person";
        Class clazz2 = Class.forName(className);

        // 第四种 使用类加载器
        ClassLoader classLoader = TestClass.class.getClassLoader();
        Class clazz3 = classLoader.loadClass(className);
    }
}
