package com.zhanghe.study.reflect;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;

/**
 * 方法测试
 */
public class TestAllMethod {
    public static void main(String[] args) {
        Class<Student> clazz = Student.class;
        System.out.println("======getDeclaredConstructors===========");
        // 获取所有的构造器
        Constructor[] declaredConstructors = clazz.getDeclaredConstructors();
        System.out.println("======getFields===========");
        // 获取所有的属性  只能获取到该类和父类中public的
        Field[] fields = clazz.getFields();
        for (Field f : fields){
            System.out.println(f.getName());
        }
        System.out.println("======getDeclaredFields===========");
        // 获取所有的属性  本类中声明的所有的属性都可以获取到，不可以获取到父类的
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field f : declaredFields){
            // 获取权限修饰符
            int i = f.getModifiers();
            String m = Modifier.toString(i);
            System.out.println("Modifier:"+m);
            // 获取属性类型
            Class type = f.getType();
            System.out.println("type："+type.getTypeName());
            // 获取属性名
            System.out.println("name:"+f.getName());
        }
        System.out.println("======getMethods===========");
        // 获取本类和父类中所有的public方法
        Method[] methods = clazz.getMethods();
        for(Method m : methods){
            System.out.println(m.getName());
        }

        System.out.println("======getDeclaredMethods===========");
        // 获取本类中所有的方法  父类的无法获取
        Method[] declaredMethods = clazz.getDeclaredMethods();
        for(Method m : declaredMethods){
            // 获取注解
            Annotation[] annotations = m.getAnnotations();
            // 权限修饰符
            System.out.println("Modifier:"+Modifier.toString(m.getModifiers()));
            // 返回值类型
            Class reture = m.getReturnType();
            // 形参列表
            Class[] params = m.getParameterTypes();
            // 方法名
            System.out.println(m.getName());
            // 异常类型
            Class[] exceptions = m.getExceptionTypes();
        }

        // 获取父类
        Class superClass = clazz.getSuperclass();

        // 获取带泛型的父类
        Type superType = clazz.getGenericSuperclass();

        // 获取父类的泛型
        if(superType instanceof ParameterizedType){ // 如果存在泛型
            Type[] actualTypeArguments = ((ParameterizedType) superType).getActualTypeArguments();
        }

        // 获取实现的接口
        Class[] interfaces  = clazz.getInterfaces();

        // 获取所在的包
        Package aPackage = clazz.getPackage();

        // 获取注解
        Annotation[] annotations  = clazz.getAnnotations();


    }
}
