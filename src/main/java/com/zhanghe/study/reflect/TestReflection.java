package com.zhanghe.study.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TestReflection {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException {
        Class<Person> clazz = Person.class;

        // 通过反射创建person实例
        Person person = clazz.newInstance();
        person.setAge(18);
        person.setCardId("123");
        person.setName("张三");
        person.display();
        // 通过反射获取类的属性，并赋值
        Field field = clazz.getDeclaredField("age");
        field.setAccessible(true);
        field.set(person,20);
        person.display();
        // 通过反射获取方法,并调用
        Method method = clazz.getMethod("display");
        method.invoke(person);
    }
}
