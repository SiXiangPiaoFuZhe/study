package com.zhanghe.study.init;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: zhanghe
 * @date: 2020/5/27 16:47
 */
public class Test1 {
    public static void main(String[] args) {
        Class clazz = List.class;
        System.out.println(clazz.isAssignableFrom(ArrayList.class));
    }

    // 普通方法和构造方法中在局部变量表中会存在一个this的变量，且占用index为0的位置
    // LocalVariableTable:
    //        Start  Length  Slot  Name   Signature
    //            0       9     0  this   Lcom/zhanghe/study/init/Test1;
    public void test(){
        System.out.println("");
    }

    public void test1(){
        int i = 1000;
//        int j = 2000;
//        double m = 200.0;
    }
}
