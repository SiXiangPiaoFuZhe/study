package com.zhanghe.study.init;

/**
 * @author zh
 * @date 2023/3/20 15:54
 */
public class Child extends Parent{
    public int num = 2;

    public static int value = 123;

    {
        System.out.println("子类代码块"+num);
    }

    static {
        System.out.println("子类静态块");
    }

    public Child(){
        System.out.println("子类构造器:"+num);
    }

    public void show(){
        System.out.println("子类show方法"+num);
    }


}
