package com.zhanghe.study.init;

/**
 * @author zh
 * @date 2023/10/20 14:20
 */
public class Test2 {
    public static void main(String[] args) {
        // 子类show方法0
        //父类代码块1
        //父类构造器:1
        //子类代码块2
        //子类构造器:2
        new Child();
    }
}
