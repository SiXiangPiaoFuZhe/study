package com.zhanghe.study.init;

/**
 * @author: zhanghe
 * @date: 2020/5/27 15:17
 */
public class Initable3 {
    public static int COUNT = 33;

    static {
        System.out.println("Initable3初始化");
    }
}
