package com.zhanghe.study.init;

/**
 * 初始化
 * @Author: zhanghe
 * @Date: 2020/4/29 17:23
 */
public class House {
    // 构造器之前
    Window w1 = new Window(1);

    House(){
        System.out.println("House()");
        Window window = new Window(11);
    }

    // 构造器之后
    Window w2 = new Window(2);

    void f(){
        System.out.println("f()");
    }

    Window w3 = new Window(3);

    static Window w4 = new Window(4);

    public static void main(String[] args) {
        House house = new House();
        house.f();
    }
}
