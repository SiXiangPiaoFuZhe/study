package com.zhanghe.study.init;

/**
 * @author: zhanghe
 * @date: 2020/5/27 15:18
 */
public class Initable4 {
    public static int COUNT = 44;

    static {
        System.out.println("Initable4初始化");
    }
}
