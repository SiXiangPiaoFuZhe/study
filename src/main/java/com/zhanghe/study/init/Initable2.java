package com.zhanghe.study.init;

/**
 * @author: zhanghe
 * @date: 2020/5/27 15:17
 */
public class Initable2 {
    public static int COUNT = 56;

    static {
        System.out.println("Initable2初始化");
    }
}
