package com.zhanghe.study.init;

/**
 * @author: zhanghe
 * @date: 2020/5/27 15:13
 */
public class Initable1 {
    public static final int COUNT = 47;

    static {
        System.out.println("Initable1初始化");
    }
}
