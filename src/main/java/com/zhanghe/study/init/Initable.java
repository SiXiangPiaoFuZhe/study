package com.zhanghe.study.init;

import java.util.Random;

/**
 * @author: zhanghe
 * @date: 2020/5/27 15:11
 */
public class Initable {
    public static final int COUNT =new Random().nextInt(1000);
    static {
        System.out.println("Initable初始化");
    }
}
