package com.zhanghe.study.init;

/**
 * @author: zhanghe
 * @date: 2020/5/27 15:12
 */
public class Test {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        System.out.println("Initable---------------");
        System.out.println(Initable.COUNT);
        System.out.println("Initable1---------------");
        System.out.println(Initable1.COUNT);
        System.out.println("Initable2---------------");
        System.out.println(Initable2.COUNT);
        System.out.println("Initable3---------------");
        Class<Initable3> clazz = Initable3.class;
        System.out.println("Initable4---------------");
        Class.forName("com.zhanghe.study.init.Initable4");
    }
}
