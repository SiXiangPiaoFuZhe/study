package com.zhanghe.study.init;

/**
 * @author zh
 * @date 2023/10/20 14:21
 */
public class TestStatic {
    public static void main(String[] args) {
        System.out.println(SubClass.value);
    }
}

class SuperClass {
    static {
        System.out.println("父静态块");
    }

    public static int value = 123;
}

class SubClass extends SuperClass {

    static {
        System.out.println("子静态块");
    }
}
