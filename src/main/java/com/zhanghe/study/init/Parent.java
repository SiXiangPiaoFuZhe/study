package com.zhanghe.study.init;

/**
 * @author zh
 * @date 2023/3/20 15:54
 */
public class Parent {

    public int num = 1;

    {
        show();
        System.out.println("父类代码块"+num);
    }

    static {
        System.out.println("父类静态块");
    }

    public Parent(){
        System.out.println("父类构造器:"+num);
    }

    public void show(){
        System.out.println("父类show方法"+num);
    }
}
