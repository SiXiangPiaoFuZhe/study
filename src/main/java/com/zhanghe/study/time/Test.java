package com.zhanghe.study.time;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

public class Test {
    public static void main(String[] args) {
        System.out.println("----------使用Calendar--------------------");
        Calendar cal = Calendar.getInstance();
        System.out.println("年"+cal.get(Calendar.YEAR));
        System.out.println("月"+(cal.get(Calendar.MONTH)+1)); // Calendar.MONTH  获取到的是0-11
        System.out.println("日"+cal.get(Calendar.DATE));
        System.out.println(cal.get(Calendar.HOUR)); // 12小时制的小时
        System.out.println("时"+cal.get(Calendar.HOUR_OF_DAY)); // 24小时制的小时
        System.out.println("分"+cal.get(Calendar.MINUTE));
        System.out.println("秒"+cal.get(Calendar.SECOND));

        System.out.println("--------------使用java8的LocalDateTime----------------");
        LocalDateTime local = LocalDateTime.now();
        System.out.println("年"+local.getYear());
        System.out.println(local.getMonth().name());  // 英文的月
        System.out.println("月"+local.getMonthValue());  // 阿拉伯 相当于local.getMonth().getValue()
        System.out.println("日"+local.getDayOfMonth());
        System.out.println("时"+local.getHour()); // 24小时制的小时
        System.out.println("分"+local.getMinute());
        System.out.println("秒"+local.getSecond());
        System.out.println("===========================时间格式化");
        DateFormat format = new SimpleDateFormat("yyyy.MM.dd E"); //2021.01.14 星期四
        System.out.println(format.format(new Date()));
        // 一年中的第几天
        format = new SimpleDateFormat("yyyy.MM.dd D"); //2021.01.14 14
        System.out.println(format.format(new Date()));
        // 一年中的第几周
        format = new SimpleDateFormat("yyyy.MM.dd w"); //2021.01.14 3
        System.out.println(format.format(new Date()));
        // 一月中的第几周
        format = new SimpleDateFormat("yyyy.MM.dd W"); //2021.01.14 3
        System.out.println(format.format(new Date()));
        // A.M./P.M.标记
        format = new SimpleDateFormat("yyyy.MM.dd a"); //2021.01.14 下午
        System.out.println(format.format(new Date()));
        // 一天中的第几个小时(1~24)
        format = new SimpleDateFormat("yyyy.MM.dd k"); //2021.01.14 14
        System.out.println(format.format(new Date()));
        // 带有A.M./P.M.的小时
        format = new SimpleDateFormat("yyyy.MM.dd K"); //2021.01.14 2
        System.out.println(format.format(new Date()));
        // 时区
        format = new SimpleDateFormat("yyyy.MM.dd z"); //2021.01.14 14
        System.out.println(format.format(new Date()));
    }
}
