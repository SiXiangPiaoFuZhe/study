package com.zhanghe.study.time;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

/**
 * 测试SimpleDateFormat线程安全问题
 * @author zh
 * @date 2021/4/28 15:17
 */
public class TestSimpleDateFormat extends Thread{
    static DateFormat format = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");

    private Date date;

    public static void main(String[] args) {
        LocalDateTime local = LocalDateTime.now();
        long time = local.toInstant(ZoneOffset.ofHours(8)).toEpochMilli();
        TestSimpleDateFormat test1 = new TestSimpleDateFormat(new Date(time));

        LocalDateTime local2 = LocalDateTime.of(2020,2,1,2,20);
        long time2 = local2.toInstant(ZoneOffset.ofHours(8)).toEpochMilli();
        TestSimpleDateFormat test2 = new TestSimpleDateFormat(new Date(time2));

        test1.start();
        test2.start();

    }

    TestSimpleDateFormat(Date date){
        this.date = date;
    }

    @Override
    public void run() {

        System.out.println(format.format(date));
    }
}
