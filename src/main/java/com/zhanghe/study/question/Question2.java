package com.zhanghe.study.question;

import java.util.ArrayList;
import java.util.List;

/**
 * 判断101-200之间有多少个素数，并输出所有素数
 *
 * @author: zhanghe
 * @date: 2020/7/4 20:07
 */
public class Question2 {
    public static void main(String[] args) {
        print();
    }

    public static void print() {

        List<Integer> list = new ArrayList<>();
        for (int i = 101; i < 200; i = i + 2) {
            boolean isZhiShu = true;
            for (int j = 2; j <= Math.sqrt(i); j++) {
                if(i % j == 0){
                    isZhiShu = false;
                    break;
                }
            }
            if(isZhiShu){
                list.add(i);
                System.out.printf("%d ",i);
            }
        }
    }
}
