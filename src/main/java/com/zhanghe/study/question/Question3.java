package com.zhanghe.study.question;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: zhanghe
 * @date: 2020/7/4 20:35
 */
public class Question3 {
    static List<Integer> list = new ArrayList<>();
    public static void main(String[] args) {
        print(15, 2);
        list.forEach(System.out :: println);

    }

    public static void print(int count, int k) {
        if (count == k) {
            list.add(k);
        } else if (count % k == 0) {
            count = count / k;
            list.add(k);
            print(count, k);
        } else {
            k = k + 1;
            print(count, k);
        }

    }
}
