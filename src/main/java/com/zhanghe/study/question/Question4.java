package com.zhanghe.study.question;

/**
 * 利用条件运算符的嵌套来完成此题：学习成绩>=90分的同学用A表示，60-89分之间
 * 的用B表示，60分以下的用C表示
 *
 * @author: zhanghe
 * @date: 2020/7/4 20:52
 */
public class Question4 {

    public static void main(String[] args) {
        scord(92);
    }

    public static void scord(int count) {
        System.out.println(
                count >= 90 ? "A" : count >= 60 ? "B" : "C"
        );
        System.out.println(12 & 10);
    }

}
