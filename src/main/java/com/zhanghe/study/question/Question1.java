package com.zhanghe.study.question;

/**
 * 有一对兔子，从出生后第3个月起每个月都生一对兔子，小兔子长到第三个月后每个月又生一对兔子，假如兔子都不死，问每个月的兔子总数为多少
 * 斐波那契数列
 * 1,1,2,3,5,8,13...
 * @author: zhanghe
 * @date: 2020/7/4 18:53
 */
public class Question1 {

    public static void main(String[] args) {
        System.out.println(count(8));
    }

    public static int count(int month){
        if(month == 1 || month == 2){
            return 1;
        } else {
            return count(month -1) + count(month -2);
        }
    }

}
