package com.zhanghe.study.question;

import java.util.ArrayList;
import java.util.List;

/**
 * 打印出所有的”水仙花数”，所谓”水仙花数”是指一个三位数，其各位数字立方和等于该数本身。
 * 例如:153是一个”水仙花数”，因为153=1的三次方＋5的三次方＋3的三次方
 *
 * @author: zhanghe
 * @date: 2020/7/4 20:22
 */
public class Question {
    public static void main(String[] args) {
        print();
    }

    public static void print() {
        for (int i = 100; i < 1000; i++) {
            int a = i / 100;
            int b = i / 10 - 10 * a;
            int c = i % 10;
            List<Integer> list = new ArrayList<>();
            if (Math.pow(a, 3) + Math.pow(b, 3) + Math.pow(c, 3) == i) {
                list.add(i);
                System.out.printf("%d ", i);
            }
        }


    }
}
