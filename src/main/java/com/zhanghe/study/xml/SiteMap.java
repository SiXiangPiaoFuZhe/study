package com.zhanghe.study.xml;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.security.AnyTypePermission;
import okhttp3.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author zh
 * @date 2023/10/10 10:32
 */
public class SiteMap {

    public static void main(String[] args) throws IOException {
        XStream xStream = new XStream(new DomDriver());

        xStream.addPermission(AnyTypePermission.ANY);
        xStream.ignoreUnknownElements();
        xStream.autodetectAnnotations(true);
        xStream.processAnnotations(UrlDatas.class);
//        xStream.alias("urlset", List.class);
//        xStream.alias("url", UrlData.class);
        UrlDatas o = (UrlDatas) xStream.fromXML(new File("/Users/zhanghe/Desktop/user/myself/source_code_of_hexo_blog/lubao_blog/public/sitemap.xml"));
        System.out.println(o);

        if (o != null) {
            List<UrlData> urlDatas = o.getUrlDatas();
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
//            for (UrlData urlData : urlDatas) {
//                Random random = new Random();
//                int count = random.nextInt(3);
//                System.out.println(urlData.getLoc());
//                for (int i = 0; i < count; i++) {
//
//                    try {
//                        Request request = new Request.Builder()
//                                .url("https://busuanzi.ibruce.info/busuanzi?jsonpCallback=BusuanziCallback_1066898836003")
//                                .method("GET", null)
//                                .addHeader("Referer", urlData.getLoc())
//                                .build();
//                        Response response = client.newCall(request).execute();
//                        response.close();
//
//                        try {
//                            Thread.sleep(100);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    } catch (Exception e){
//                        e.printStackTrace();
//                    }
//
//
//                }
//            }

            StringBuilder sb = new StringBuilder();
            for(UrlData urlData : urlDatas){
                sb.append(urlData.getLoc()).append("\n");
//                locs.add(urlData.getLoc());
            }
            System.out.println(sb);
//            RequestBody requestBody = RequestBody.create(
//                    sb.toString(), MediaType.get("text/plain")
//            );
//            Request request = new Request.Builder()
//                    .url("http://data.zz.baidu.com/urls?site=zhhll.icu&token=jiVXnUNTXTKpH2gM")
//                    .method("post",requestBody)
//                    .build();
//            Response response = client.newCall(request).execute();
//            response.close();

        }
    }
}
