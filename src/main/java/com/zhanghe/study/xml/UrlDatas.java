package com.zhanghe.study.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zh
 * @date 2023/10/10 11:23
 */
@XStreamAlias("urlset")
public class UrlDatas {
    @XStreamImplicit(itemFieldName = "url")
    private List<UrlData> urlDatas = new ArrayList<>();

    public List<UrlData> getUrlDatas() {
        return urlDatas;
    }

    public void setUrlDatas(List<UrlData> urlDatas) {
        this.urlDatas = urlDatas;
    }
}
