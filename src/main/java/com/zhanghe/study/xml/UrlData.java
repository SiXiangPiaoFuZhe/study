package com.zhanghe.study.xml;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * @author zh
 * @date 2023/10/10 10:31
 */
@XStreamAlias("url")
public class UrlData {
    @XStreamAsAttribute
    private String loc;

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }
}
