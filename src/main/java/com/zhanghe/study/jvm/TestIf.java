package com.zhanghe.study.jvm;

/**
 * @author zh
 * @date 2021/7/13 14:49
 */
public class TestIf {
    public void test(){
        int a = 10;
        int b = 20;
        // if_icmpne
        if(a == b){
            System.out.println("相等");
        } else {
            System.out.println("不相等");
        }
    }
}
