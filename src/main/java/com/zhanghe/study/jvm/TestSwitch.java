package com.zhanghe.study.jvm;

/**
 * @author zh
 * @date 2021/7/13 14:55
 */
public class TestSwitch {
    public void test(){
        int result;
        int i = 10;
        // tableswitch
        switch (i){
            case 1:
                result = 10;
                break;
            case 2:
                result = 20;
                break;
            case 3:
                result = 30;
                break;
            default:
                result = 0;
        }
    }

    public void test1(){
        int result;
        int i = 10;
        //  lookupswitch
        switch (i){
            case 100:
                result = 10;
                break;
            case 200:
                result = 20;
                break;
            case 300:
                result = 30;
                break;
            default:
                result = 0;
        }
    }
}
