package com.zhanghe.study.jvm;

/**
 * @author zh
 * @date 2021/7/14 15:09
 */
public class TestClinit {
    public static void main(String[] args) {
        // 此时不会触发 AAA类初始化
//        System.out.println(AAA.i);
        // 此时会触发 AAA类初始化
        System.out.println(AAA.bb);
    }
}

class AAA {
    public static final int i = 10;
    public static final BBB bb = new BBB();
    static{
        System.out.println("AAA类初始化");
    }
}

class BBB {

}
