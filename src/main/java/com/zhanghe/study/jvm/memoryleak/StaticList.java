package com.zhanghe.study.jvm.memoryleak;

import java.util.ArrayList;
import java.util.List;

/**
 * 内存泄漏 静态集合
 * @author zh
 * @date 2021/7/23 10:10
 */
public class StaticList {
    static List<Object> list = new ArrayList<>();

    public void test(){
        // 由于list是静态的，与JVM的生命周期相同，所以obj对象不会被回收
        Object obj = new Object();
        list.add(obj);
    }
}
