package com.zhanghe.study.jvm;

/**
 * @author zh
 * @date 2021/7/7 11:36
 */
public class TestLoad {
    // 局部变量压栈
    public void loadN(int i,long l,APP app){
        // iload_1   从LocalVariableTable可以看到，0位是this
        System.out.println(i);
        // lload_2
        System.out.println(l);
        // aload         4
        System.out.println(app);
    }

    static class APP{

    }

    //常量入栈
    public void pushConstLdc(){
        // iconst_5
        int a = 5;
        // bipush        127
        int b = 127;
        // sipush        128
        int c = 128;

        // ldc           #6                  // int 32768
        int f = 32768;
        // ldc           #7                  // String 123
        String s = "123";

        // ldc2_w        #8                  // long 20l
        long l = 20;
        // ldc           #10                 // float 20.2f
        float f1 = 20.2f;
        // ldc2_w        #11                 // double 20.12d
        double d = 20.12;

    }

    // 出栈装入局部变量表
    public void store(int i,double d){
        //  0: iload_1   加载i变量入参
        //  1: iconst_2  常量2
        //  2: iadd      相加操作
        //  3: istore        4   将得到的数值放入局部变量表中索引为4的位置(由于d为double类型，占用了两个位置)
        int m = i+2;
//        5: ldc2_w        #13                 // double 100.0d
//        8: dstore_2  // 将double类型放入局部变量表第二个索引位置
        d = 100;
    }

    // i++和++i的区别
    public void add(int i){
//        0: iload_1   操作数栈加载i变量
//        1: iinc          1, 1  执行自增运算
//        4: istore_2   将操作数栈中的栈顶存储到局部变量表中
        int a = i++;
        System.out.println(a);
//        11: iinc          1, 1  执行自增运算
//        14: iload_1       操作数栈加载i变量
//        15: istore_3      将操作数栈中的栈顶存储到局部变量表中
        int b = ++i;
    }
}
