package com.zhanghe.study.jvm;

/**
 * @author zh
 * @date 2022/5/15 17:10
 */
public class Test1 {

    int a;
    void Test1() {
        a = 10;
    }

    public static void main(String[] args) {
        Test1 s = new Test1();
        System.out.println(s.a);
    }
}
