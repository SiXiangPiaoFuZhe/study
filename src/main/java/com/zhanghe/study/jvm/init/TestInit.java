package com.zhanghe.study.jvm.init;

/**
 * @author zh
 * @date 2022/4/12 15:58
 */
public class TestInit {
    public static void main(String[] args) {
        // super init
        // com.zhanghe.study.jvm.init.AA@5ca881b5
        System.out.println(Sub.a);
    }
}

class Super{
    static {
        System.out.println("super init");
    }

    public static AA a = new AA();
}

class Sub extends Super{
    static {
        System.out.println("sub init");
    }
}

class AA{

}
