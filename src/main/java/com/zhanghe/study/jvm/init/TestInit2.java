package com.zhanghe.study.jvm.init;

/**
 * @author zh
 * @date 2022/6/5 14:03
 */
public class TestInit2 {
    private static TestInit2 testLoad = new TestInit2();
    private static int value1;
    private static int value2 = 0;

    public TestInit2(){
        value1 = 10;
        value2 = value1;
        System.out.println("before value1="+value1);
        System.out.println("before value2="+value2);

    }

    public static void main(String[] args) {
        System.out.println("after value1="+value1);
        System.out.println("after value2="+value2);
    }
}
