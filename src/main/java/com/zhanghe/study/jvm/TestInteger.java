package com.zhanghe.study.jvm;

/**
 * @author zh
 * @date 2022/5/14 10:36
 */
public class TestInteger {
    public static void main(String[] args) {
        Integer a = 2;
        Integer b = 2;
        Integer c = 200;
        Integer d = 200;

        System.out.println(a == b); // true
        System.out.println(c == d); // false
    }
}
