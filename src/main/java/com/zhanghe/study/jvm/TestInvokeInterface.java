package com.zhanghe.study.jvm;

/**
 * invokeInterface指令测试
 * @author zh
 * @date 2021/7/13 10:34
 */
public class TestInvokeInterface {
    public static void main(String[] args) {
        // invokespecial
        InvokeDao invokeDao = new InvokeDaoImpl();
        // invokeinterface
        invokeDao.test();
        // 接口的默认方法 invokeinterface
        invokeDao.testDefault();
        // 接口的静态方法 invokestatic
        InvokeDao.testStatic();
    }
}

interface InvokeDao{

    String test();

    public static void testStatic(){

    }

    public default void testDefault(){

    }

}

class InvokeDaoImpl implements InvokeDao{

    @Override
    public String test() {
        return null;
    }
}
