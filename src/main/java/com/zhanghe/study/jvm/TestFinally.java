package com.zhanghe.study.jvm;

/**
 * @author zh
 * @date 2021/7/13 17:18
 */
public class TestFinally {
    public static void main(String[] args) {
        // hello
        System.out.println(test());
        // hi
        System.out.println(test1());
    }

    public static String test(){
        String str = "hello";
        try{
            return str;
        } finally {
            str = "hi";
        }
    }

    public static String test1(){
        try{
            return "hello";
        } finally {
            return "hi";
        }
    }
}
