package com.zhanghe.study.jvm;

import org.openjdk.jol.info.ClassLayout;

/**
 * @author zh
 * @date 2021/7/13 11:36
 */
public class TestDup {
    private int index = 0;
    public int test(){
        return index++;
    }

    public static void main(String[] args) {
        System.out.println(ClassLayout.parseClass(TestDup.class).toPrintable());

    }
}
