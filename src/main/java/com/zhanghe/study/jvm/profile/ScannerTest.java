package com.zhanghe.study.jvm.profile;

import java.util.Scanner;

/**
 * @author zh
 * @date 2021/7/21 10:14
 */
public class ScannerTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.nextInt();
    }
}
