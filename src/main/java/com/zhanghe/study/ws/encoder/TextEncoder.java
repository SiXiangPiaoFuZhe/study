package com.zhanghe.study.ws.encoder;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 * @author zh
 * @date 2021/11/7 15:25
 */
public class TextEncoder implements Encoder.Text<Message>{
    // 实现将对象转为字符串(可以json，可以xml)
    @Override
    public String encode(Message o) throws EncodeException {
        return null;
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
