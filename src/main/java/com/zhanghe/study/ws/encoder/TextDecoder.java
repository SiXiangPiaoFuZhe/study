package com.zhanghe.study.ws.encoder;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

/**
 * @author zh
 * @date 2021/11/7 15:29
 */
public class TextDecoder implements Decoder.Text<Message>{
    @Override
    public Message decode(String s) throws DecodeException {
        return null;
    }

    @Override
    public boolean willDecode(String s) {
        return false;
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
