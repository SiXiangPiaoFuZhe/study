package com.zhanghe.study.ws;

import com.zhanghe.study.ws.encoder.TextDecoder;
import com.zhanghe.study.ws.encoder.TextEncoder;

import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 * 注解方式创建端点
 * @author zh
 * @date 2021/11/7 15:17
 */
@ServerEndpoint(value = "/echo",
        // 添加编码器
        encoders = {
        TextEncoder.class
        },
        // 添加解码器
        decoders = {
                TextDecoder.class
        }
)
public class EchoEndpoint {

    @OnMessage
    public void onMessage(Session session,String msg){

    }
}
