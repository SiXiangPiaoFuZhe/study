package com.zhanghe.study.ws;

import javax.websocket.*;
import java.io.IOException;

/**
 * 编程方式创建端点
 * @author zh
 * @date 2021/11/7 15:08
 */
public class TestWebSocket extends Endpoint {
    // Endpoint 有三个生命周期方法onOpen、onClose、onError
    // Session参数表示这个端点和远端端点之间的会话
    @Override
    public void onOpen(Session session, EndpointConfig endpointConfig) {
        // 注册消息处理器，端点接收到消息，会执行消息处理器的onMessage方法
        session.addMessageHandler(new MessageHandler.Whole<String>() {
            @Override
            public void onMessage(String s) {
                try {
                    System.out.println("收到消息"+s);
                    // session.getBasicRemote() 会返回一个表示远端端点的对象
                    session.getBasicRemote().sendText("收到");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onClose(Session session, CloseReason closeReason) {
        super.onClose(session, closeReason);
    }

    @Override
    public void onError(Session session, Throwable thr) {
        super.onError(session, thr);
    }
}
