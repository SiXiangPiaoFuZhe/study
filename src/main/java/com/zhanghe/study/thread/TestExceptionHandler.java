package com.zhanghe.study.thread;

/**
 * @author zh
 * @date 2020/12/1 16:37
 */
public class TestExceptionHandler {
    public static void main(String[] args) {
        TestExceptionHandler test = new TestExceptionHandler();
        Thread thread = new Thread(test.new MyThread());
        thread.setUncaughtExceptionHandler(new MyExceptionHandler());
        thread.start();

    }

    class MyThread implements Runnable {

        @Override
        public void run() {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int i = 1 / 0;
        }
    }

}


class MyExceptionHandler implements Thread.UncaughtExceptionHandler {

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        System.out.println(t.getName() + "抛出异常" + e.getMessage());
        e.printStackTrace();
    }
}
