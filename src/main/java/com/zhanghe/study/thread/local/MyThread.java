package com.zhanghe.study.thread.local;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @Author: zhanghe
 * @Date: 2020/4/13 9:44
 */
public class MyThread implements Runnable {
    public static void main(String[] args) {
        try{
            throw new IOException();
        } catch (FileNotFoundException e){
            System.out.println("FileNotFound");
        } catch (IOException e){
            System.out.println("IO");
        } catch (Exception e){
            System.out.println("exception");
        }
        Object o = 1;
        String s = String.valueOf(o);
        System.out.println(s);
    }

    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            ThreadLocalVariableHolder.increment();
            System.out.println(Thread.currentThread().getName() + ":" + ThreadLocalVariableHolder.get());
            Thread.yield();
        }
    }
}
