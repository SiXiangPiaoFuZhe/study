package com.zhanghe.study.thread.test_001;

/**
 * 如果不使用synchronized会出现count打印不对的问题
 * @author: zhanghe
 * @date: 2020/6/22 15:21
 */
//不使用synchronized的情况
//Thread-2------8
//Thread-1------6
//Thread-4------5
//Thread-0------8
//Thread-3------7
public class TestSynchronized implements Runnable {

    private int count = 10;

    @Override
    public synchronized void run() {
        count--;
        System.out.println(Thread.currentThread().getName() + "------" + count);
    }

    public static void main(String[] args) {
        TestSynchronized testSynchronized = new TestSynchronized();
        for (int i = 0;i<5;i++){
            new Thread(testSynchronized).start();
        }
    }
}
