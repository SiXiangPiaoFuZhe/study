package com.zhanghe.study.thread.test_001;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author: zhanghe
 * @date: 2020/6/22 11:07
 */
public class TestAtomic {
    private AtomicInteger count = new AtomicInteger(0);
    public static void main(String[] args) throws InterruptedException {
        TestAtomic test = new TestAtomic();
        Thread t1 = new Thread(()->{
            test.mm();
        });
        Thread t2 = new Thread(()->{
            test.mm();
        });
        t1.start();
        t2.start();
        Thread.sleep(3000);
        System.out.println(test.count);
    }



    public void mm(){
        for(int i = 0;i<100;i++){
            count.incrementAndGet();
        }
    }
}
