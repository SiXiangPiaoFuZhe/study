package com.zhanghe.study.thread.test_001;

/**
 * @author: zhanghe
 * @date: 2020/6/22 10:43
 */
public class Test {

    public static void main(String[] args) {
        Test test = new Test();
        Thread t1 = new Thread(()->{
            test.mm();
        });
        Thread t2 = new Thread(()->{
            test.mm();
        });
        t1.start();
        t2.start();
    }

    public void mm(){
        for(int i = 0;i<100;i++){
            System.out.print(i+" ");
        }
    }
}
