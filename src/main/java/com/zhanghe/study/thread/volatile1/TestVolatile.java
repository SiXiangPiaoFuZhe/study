package com.zhanghe.study.thread.volatile1;


/**
 * @author zh
 * @date 2021/2/6 10:42
 */
public class TestVolatile {

    public static void main(String[] args) {
        ThreadDemo runnable = new ThreadDemo();
        new Thread(runnable).start();

        while (true){
            // 不会停止  一直在while循环
            if(runnable.isFlag()){
                System.out.println("----------");
                break;
            }
        }
    }

}

class ThreadDemo implements Runnable{

    private boolean flag;
    @Override
    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        flag = true;

        System.out.println(Thread.currentThread().getName()+"---flag:"+flag);
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}


