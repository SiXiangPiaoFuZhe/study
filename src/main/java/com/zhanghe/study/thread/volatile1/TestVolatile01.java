package com.zhanghe.study.thread.volatile1;


/**
 * @author zh
 * @date 2021/2/6 10:42
 */
public class TestVolatile01 {

    public static void main(String[] args) {
        ThreadDemo01 runnable = new ThreadDemo01();
        new Thread(runnable).start();

        while (true){
            // 不会停止  一直在while循环
            if(runnable.isFlag()){
                System.out.println("----------");
                break;
            }
        }
    }

}

class ThreadDemo01 implements Runnable{

    private volatile boolean flag;
    @Override
    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        flag = true;

        System.out.println(Thread.currentThread().getName()+"---flag:"+flag);
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}


