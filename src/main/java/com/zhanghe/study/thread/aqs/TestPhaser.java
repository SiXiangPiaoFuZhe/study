package com.zhanghe.study.thread.aqs;

import java.util.Random;
import java.util.concurrent.Phaser;

/**
 * @author zh
 * @date 2022/1/6 16:49
 */
public class TestPhaser {

    public static Phaser phaser = new Phaser(3);

    public static void main(String[] args) {
        Runner runner = new Runner();
        for(int i=0;i<3;i++){
            Thread thread = new Thread(runner);
            thread.start();
        }
    }

    public static void method(){
        System.out.println(Thread.currentThread().getName()+"-A1 begin");
        phaser.arriveAndAwaitAdvance();
        System.out.println(Thread.currentThread().getName()+"-A1 end");
    }

    static class Runner implements Runnable{

        @Override
        public void run() {
            try {
                Thread.sleep(new Random().nextInt(10000));
                TestPhaser.method();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
