package com.zhanghe.study.thread.aqs;

import java.util.concurrent.locks.LockSupport;

/**
 * @author zh
 * @date 2021/12/5 13:17
 */
public class TestLockSupport {
    public static void main(String[] args) {
        System.out.println("begin");

        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("开始执行了，阻塞。。。");
                LockSupport.park();
                System.out.println("被唤醒了。。");
            }
        };
        thread.start();
        try {
            System.out.println("我先休息会在唤醒你");
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("准备唤醒");
        LockSupport.unpark(thread);
    }
}
