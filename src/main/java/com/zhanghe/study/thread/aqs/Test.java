package com.zhanghe.study.thread.aqs;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author zh
 * @date 2022/1/6 15:36
 */
public class Test {
    private volatile boolean start = false;

    private volatile long startTime;

    private volatile AtomicInteger rank = new AtomicInteger(0);

    public boolean isStart() {
        return start;
    }

    public int getRank(){
        return rank.incrementAndGet();
    }

    public static void main(String[] args) {
        Test test = new Test();
        for(int i = 0;i<3;i++){
            PlayerGroup playerGroup = new PlayerGroup(test,"【队伍"+i+"】");
            playerGroup.start();
        }
        try {
            Thread.sleep(1000);
            test.start = true;
            test.startTime = System.currentTimeMillis();
            System.out.println("==========比赛开始=============");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    // 一组运动员 4个运动员
    static class PlayerGroup extends Thread{
        private Player[] players = new Player[4];
        private String name; // 队名
        private AtomicInteger count = new AtomicInteger(0);
        private Test test;


        public PlayerGroup(Test test, String name){
            this.test = test;
            this.name = name;
            for(int i = 0;i<players.length;i++){
                Player player = new Player(name,i+1,count);
                players[i] = player;
                Thread thread = new Thread(player);
                thread.start();
            }
        }

        @Override
        public void run() {
            boolean ok = false;
            for(;;){
                if (count.get() == 4){
                    System.out.println(name+"进入赛场");
                    ok = true;
                    break;
                }
                if(test.isStart()){
                    System.out.println("比赛已经开始，但是人员不足，队伍"+name+"离场");
                    break;
                }
            }
            if(!ok){
                return;
            }
            for(;;){
                if (test.isStart()){
                    // 比赛开始，接力跑
                    for(Player player : players){
                        player.race();
                    }
                    System.out.println("队伍"+name+"跑完全程，用时"+(System.currentTimeMillis()- test.startTime)+"ms,名次"+ test.getRank());
                    break;
                }
            }

        }
    }

    // 一个运动员
    static class Player implements Runnable{
        private int num; // 跑第几棒
        private AtomicInteger count;
        private String name;
        public Player(String name,int num,AtomicInteger count){
            this.name = name;
            this.num = num;
            this.count=count;
        }
        // 进入赛场
        public void enterGround(){
            try {
                Thread.sleep(new Random().nextInt(1100));
                count.incrementAndGet();
                System.out.println("队伍"+name+"队员"+num+"进场"+count.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // 开始跑
        public void race(){
            try {
                Thread.sleep(new Random().nextInt(10000));

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            enterGround(); // 进入赛场
        }
    }
}
