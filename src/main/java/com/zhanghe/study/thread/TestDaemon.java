package com.zhanghe.study.thread;

/**
 * 测试守护线程和非守护线程
 * @author zh
 * @date 2021/1/22 11:44
 */
public class TestDaemon {
    public static void main(String[] args) {
        Thread thread = new Thread(new MyThread());
        // 设置为true为守护线程 当主线程停止时，该线程也会停止
        // 设置为false为非守护线程(用户线程) 当主线程停止时，该线程继续运行
        thread.setDaemon(false);
        thread.start();
        System.out.println("结束");
    }

    static class MyThread implements Runnable{

        @Override
        public void run() {
            for(int i = 0;i<1000;i++){
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(i);
            }
        }
    }
}

