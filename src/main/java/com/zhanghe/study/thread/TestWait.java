package com.zhanghe.study.thread;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author zh
 * @date 2022/2/28 16:55
 */
public class TestWait {
    public static void main(String[] args) throws InterruptedException {
        TestWait wait = new TestWait();
        Lock lock = new ReentrantLock();
        lock.lock();
        wait.wait(); // IllegalMonitorStateException
    }
}
