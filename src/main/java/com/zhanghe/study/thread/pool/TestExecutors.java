package com.zhanghe.study.thread.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author zh
 * @date 2021/4/2 10:08
 */
public class TestExecutors {
    public static void main(String[] args) {
//        testSingleThread();
//        testFixedThread();
//        testCachedThread();
        testScheduledThread();
    }

    // 测试newSingleThreadExecutor线程池
    private static void testSingleThread() {
        // 该线程池中只有一个线程 一直在复用这个线程
        ExecutorService pool = Executors.newSingleThreadExecutor();
        for(int i = 0;i<5;i++){
            int finalI = i;

            pool.execute(() ->
                    {
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if(finalI == 3){
                            throw new RuntimeException("出错了");
                        }
                        System.out.println(Thread.currentThread().getName() + ":"+ finalI);
                    });
        }
        pool.shutdown();
    }

    // 测试newFixedThreadPool线程池
    private static void testFixedThread() {
        // 创建固定大小的线程池
        ExecutorService pool = Executors.newFixedThreadPool(2);
        for(int i = 0;i<5;i++){
            int finalI = i;

            pool.execute(() ->
            {
                if(finalI == 3){
                    throw new RuntimeException("出错了");
                }
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println(Thread.currentThread().getName() + ":"+ finalI);
            });
        }
        pool.shutdown();
    }


    // 测试newCachedThreadPool线程池
    private static void testCachedThread() {

        ExecutorService pool = Executors.newCachedThreadPool();
        for(int i = 0;i<5;i++){
            int finalI = i;

            pool.execute(() ->
            {
                if(finalI == 3){
                    throw new RuntimeException("出错了");
                }
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println(Thread.currentThread().getName() + ":"+ finalI);
            });
        }
        pool.shutdown();
    }

    // 测试newScheduledThreadPool线程池
    private static void testScheduledThread() {

        ScheduledExecutorService pool = Executors.newScheduledThreadPool(10);
        for(int i = 0;i<5;i++){
            int finalI = i;

            pool.schedule(()->
            {
                if(finalI == 3){
                    throw new RuntimeException("出错了");
                }

                System.out.println(Thread.currentThread().getName() + ":"+ finalI);

            },20, TimeUnit.SECONDS);
        }
        pool.shutdown();
    }
}
