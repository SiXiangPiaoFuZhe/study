package com.zhanghe.study.thread.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author: zhanghe
 * @date: 2020/6/11 11:56
 */
public class TestExcutor {
    public static void main(String[] args) throws InterruptedException {
        System.out.println(
                Runtime.getRuntime().availableProcessors());
        ExecutorService executorService = Executors.newWorkStealingPool();
        for (int i = 0; i < 10; i++) {
            executorService.execute(new NewThread());
        }
        executorService.awaitTermination(100, TimeUnit.SECONDS);
    }

    static class NewThread implements Runnable {

        @Override
        public void run() {
            System.out.println(
                    Thread.currentThread().getName());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
