package com.zhanghe.study.thread.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author zh
 * @date 2022/1/6 18:12
 */
public class TestShutdown {
    public static void main(String[] args) {
        // 创建固定大小的线程池
        ExecutorService pool = Executors.newFixedThreadPool(2);
        for(int i = 0;i<1;i++){

            pool.execute(() ->
            {
                int num = 0;
                while (true){
                    if(!Thread.currentThread().isInterrupted()){
                        num++;
                        if(num % 1000 == 0){
                            System.out.println("---");
                        }
                    } else {
                        System.out.println("被中断了");
                        break;
                    }

                }


            });
        }
        pool.shutdownNow();
    }
}
