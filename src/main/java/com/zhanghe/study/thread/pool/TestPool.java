package com.zhanghe.study.thread.pool;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author zh
 * @date 2021/11/29 14:08
 */
public class TestPool {
    private static final int COUNT_BITS = Integer.SIZE - 3;
    private static final int CAPACITY   = (1 << COUNT_BITS) - 1;

    // 表示运行中的状态标识 runState is stored in the high-order bits
    private static final int RUNNING    = -1 << COUNT_BITS;
    // 表示关闭中的状态标识
    private static final int SHUTDOWN   =  0 << COUNT_BITS;
    // 表示已停止的状态标识
    private static final int STOP       =  1 << COUNT_BITS;
    // 表示当前所有任务已经终止，任务数量为0时的状态标识
    private static final int TIDYING    =  2 << COUNT_BITS;
    // 表示线程池已经完全终止（关闭）
    private static final int TERMINATED =  3 << COUNT_BITS;

    private static int ctlOf(int rs, int wc) { return rs | wc; }

    private final AtomicInteger ctl = new AtomicInteger(ctlOf(RUNNING, 0));
    public static void main(String[] args) {
        System.out.println(COUNT_BITS);
        System.out.println(CAPACITY);
        System.out.println(RUNNING);
        System.out.println(SHUTDOWN);
        System.out.println(STOP);
        System.out.println(TIDYING);
        System.out.println(TERMINATED);
        System.out.println(ctlOf(RUNNING,0));
    }
}
