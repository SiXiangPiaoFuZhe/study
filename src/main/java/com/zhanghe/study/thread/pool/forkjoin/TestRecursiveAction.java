package com.zhanghe.study.thread.pool.forkjoin;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

/**
 * RecuriveAction   任务没有返回结果且仅执行一次任务
 * @author zh
 * @date 2022/1/10 10:21
 */
public class TestRecursiveAction {

    public static void main(String[] args) {
        ForkJoinPool pool = new ForkJoinPool();
        pool.submit(new MyRecursiveAction(10,20));
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    static class MyRecursiveAction extends RecursiveAction {

        private int begin;
        private int end;

        public MyRecursiveAction(int begin,int end){
            this.begin = begin;
            this.end = end;
        }

        @Override
        protected void compute() {
            if(end - begin > 2){ // fork分解进行计算
                int mid = (begin + end) / 2;
                MyRecursiveAction leftAction = new MyRecursiveAction(begin,mid);
                MyRecursiveAction rightAction = new MyRecursiveAction(mid+1,end);
                invokeAll(leftAction,rightAction);
            } else {
                System.out.println(Thread.currentThread().getName()+"===计算值为"+begin+"-"+end);
            }

        }
    }
}
