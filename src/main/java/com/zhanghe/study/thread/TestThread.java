package com.zhanghe.study.thread;

// 模拟窗口售票

/**
 * 继承Thread 模拟窗口售票
 * 多个线程同时操作一个资源存在线程安全问题
 */
public class TestThread {
    public static void main(String[] args) {
        Window window1 = new Window();
        window1.setName("窗口1");
        Window window2 = new Window();
        window2.setName("窗口2");
        Window window3 = new Window();
        window3.setName("窗口3");
        Window window4 = new Window();
        window3.setName("窗口4");
        window1.start();
        window2.start();
        window3.start();
        window4.start();
    }

}

class Window extends Thread{
    static int ticket = 120;
    @Override
    public void run() {
        while (true){
            if(ticket > 0){
                System.out.println(Thread.currentThread().getName() + "卖票，票号"+ticket--);
            } else {
                break;
            }
        }

    }
}
