package com.zhanghe.study.thread.atomic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author zh
 * @date 2021/11/1 11:39
 */
public class Main {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(1);
        System.out.println(atomicInteger.get());
        System.out.println(atomicInteger.incrementAndGet());
        System.out.println(atomicInteger.getAndIncrement());

        System.out.println(atomicInteger.getAndAdd(10));
        System.out.println(atomicInteger.addAndGet(10));

        int i = 10;
        int j;
        j = i+=10;
        System.out.println(j);
        j = i=+10;
        System.out.println(j);
    }
}
