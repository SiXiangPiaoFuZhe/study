package com.zhanghe.study.thread.atomic;

import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * @author zh
 * @date 2021/12/23 15:10
 */
public class ABATest {

    public static void main(String[] args) {
        int stamp = 1;
        AtomicStampedReference<Integer> stampedReference = new AtomicStampedReference<>(0,stamp);

        stampedReference.compareAndSet(0,1,stamp,stamp+1);

        System.out.println(stampedReference.getReference());
        System.out.println(stampedReference.getStamp());
    }
}
