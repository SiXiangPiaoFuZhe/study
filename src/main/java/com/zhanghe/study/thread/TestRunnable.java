package com.zhanghe.study.thread;

/**
 * 实现Runnable接口创建线程
 * 适合多个线程操作同一个资源
 * 多个线程同时操作一个资源存在线程安全问题
 */
public class TestRunnable{
    public static void main(String[] args) {
        WindowRunnable runnable = new WindowRunnable();
        Thread thread1 = new Thread(runnable,"窗口一");
        Thread thread2 = new Thread(runnable,"窗口二");
        Thread thread3 = new Thread(runnable,"窗口三");
        thread1.start();
        thread2.start();
        thread3.start();
    }

}

class WindowRunnable implements Runnable{
    private int ticket = 100;
    @Override
    public void run() {
        while(true){
            if(ticket > 0){
                System.out.println(Thread.currentThread().getName()+"售票，票号"+ticket--);
            } else {
                break;
            }
        }
    }
}


