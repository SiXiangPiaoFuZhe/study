package com.zhanghe.study.thread;

public class ThreadDemo {
    public static void main(String[] args) throws InterruptedException {
        T1 t1 = new T1();
        Thread t = new Thread(t1);
        t.start();
        t.join();
        System.out.println("主线程执行");
    }

    static class T1 implements  Runnable{

        @Override
        public void run() {

            System.out.println("T1开始执行");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("T1执行完成");

        }
    }
}
