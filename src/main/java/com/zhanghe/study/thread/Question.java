package com.zhanghe.study.thread;

/**
 * 两个线程
 * 一个线程打ABC
 * 一个线程打123
 * 控制台打印出来A1B2
 *
 * @author: zhanghe
 * @date: 2020/6/21 17:51
 */
public class Question {

    public static void main(String[] args) throws InterruptedException {

        Object o = new Object();

        Thread t1 = new Thread(() -> {
            synchronized (o){
                for (char a = 'A'; a <= 'Z'; a++) {
                    if(a == 'Z'){
                        System.out.print(a);
                        o.notify();
                    } else {
                        System.out.print(a);
                        o.notify();
                        try {
                            o.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        });

        Thread t2 = new Thread(() -> {
            synchronized (o){
                try {
                    o.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (int a = 1; a <= 26; a++) {
                    if(a == 26){
                        System.out.print(a);
                    } else {
                        System.out.print(a);
                        o.notify();
                        try {
                            o.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }

        });

        t2.start();
        t1.start();

    }
}
