package com.zhanghe.study.thread;

import java.util.Calendar;

public class DemoApplication {

    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2019);
        calendar.set(Calendar.MONTH, 9); // 10月
        calendar.set(Calendar.DAY_OF_MONTH, 25);

        calendar.add(Calendar.DAY_OF_YEAR, 99);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int date = calendar.get(Calendar.DATE);
        System.out.println(year + "年" + month + "月" + date + "日");

    }

}
