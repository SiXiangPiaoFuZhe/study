package com.zhanghe.study.thread.queue;

import java.util.LinkedList;
import java.util.List;

public class BlockingQueueDemo {
    private List queue = new LinkedList();

    private int limit = 10;

    public BlockingQueueDemo(int limit) {
        this.limit = limit;
    }

    //添加元素
    public synchronized void enqueue(Object item) throws InterruptedException {
        while (this.queue.size() == this.limit) {
            wait();
        }
        if (this.queue.size() == 0) {
            notifyAll();
        }
        this.queue.add(item);
    }

    //移除元素
    public synchronized Object dequeue() throws InterruptedException {
        while (this.queue.size() == 0) {
            wait();
        }
        if (this.queue.size() == this.limit) {
            notifyAll();
        }
        return this.queue.remove(0);
    }
}
