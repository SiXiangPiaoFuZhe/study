package com.zhanghe.study.string;

/**
 * @author: zhanghe
 * @date: 2020/6/14 15:01
 */
public class TestString {
    public static void main(String[] args) {
//        String ss = "zxc";
        String s = new String("zxc");
        System.out.println(2 << 3);

        String s1 = "aa";
        String s2 = "bb";
        String s3 = "aabb";
        String s4 = s1+s2;
        String s5 = "aa"+"bb";
        String s6 = s4.intern();
        System.out.println(s3 == s4);// false
        System.out.println(s3 == s5);// true
        System.out.println(s3 == s6);// true
        System.out.println(s4 == s5);// false
        System.out.println(s4 == s6);// false
        System.out.println(s5 == s6);// true
//
//
        String s7 = null;
        System.out.println("---"+(s7 instanceof String)); // false
    }
}
