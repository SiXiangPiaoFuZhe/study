package com.zhanghe.study.string;

/**
 * @author zh
 * @date 2021/1/9 21:55
 */
public class TestSwitch {
    public static void main(String[] args) {
        TestSwitch testSwitch = new TestSwitch();
        testSwitch.testSwitch("男");
    }

    public void testSwitch(String gender){
        switch (gender){
            case "男":
                System.out.println("男");
                break;
            case "女":
                System.out.println("女");
                break;
            default:
                System.out.println("未知");
        }
    }
}
