package com.zhanghe.study.string;

/**
 * @author zh
 * @date 2021/6/17 10:46
 */
public class TestString1 {
    public static void main(String[] args) throws InterruptedException {
        String s1 = "aa";
        System.out.println(s1.hashCode());
        s1 = "bb";
        System.out.println(s1.hashCode());
        Thread.sleep(1000000);
    }
}
