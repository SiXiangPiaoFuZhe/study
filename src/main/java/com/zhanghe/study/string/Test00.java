package com.zhanghe.study.string;

import java.util.HashSet;
import java.util.Set;

/**
 * @author zh
 * @date 2023/12/25 11:24
 */
public class Test00 {

    public static void main(String[] args) {
        Set<String> sets = new HashSet<>();
        sets.add("1");
        sets.add("2");
        testOs(sets.toArray());
    }

    public static void testOs(Object ... obs){
        System.out.println();
    }

    public static void testOs(Set<Object>  obs){
        System.out.println();
    }
}
