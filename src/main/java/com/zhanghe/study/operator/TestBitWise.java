package com.zhanghe.study.operator;

/**
 * 位运算符
 */
public class TestBitWise {
    public static void main(String[] args) {
        int n = 10 - 1;
        System.out.println(Integer.toBinaryString(n));
        // 右移一位，在进行按位或
        n |= n >>> 1;
        System.out.println(Integer.toBinaryString(n));
        n |= n >>> 2;
        System.out.println(Integer.toBinaryString(n));
        n |= n >>> 4;
        System.out.println(Integer.toBinaryString(n));
        n |= n >>> 8;
        System.out.println(Integer.toBinaryString(n));
        n |= n >>> 16;
        System.out.println(Integer.toBinaryString(n));
        System.out.println(n+1);

    }
}
