package com.zhanghe.study.operator;

/**
 * 操作符
 * @Author: zhanghe
 * @Date: 2020/4/28 10:34
 */
public class TestEquals {
    public static void main(String[] args) {
        Value v1 = new Value();
        Value v2 = new Value();
        v1.i = 100;
        v2.i = 100;
        for (int i = 0; judge(i); i++,flow())
            System.out.println("i的值为" + i);
    }

    private static boolean judge(int i) {
        System.out.println("判断大小" + i);
        return i < 10;
    }

    private static void flow() {
        System.out.println("flow");
    }
}
