package com.zhanghe.study.lambda;

/**
 * @author: zhanghe
 * @date: 2020/5/12 0:25
 */
@FunctionalInterface
public interface Callable {
    void call(String msg);
}
