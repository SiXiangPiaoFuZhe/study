package com.zhanghe.study.lambda;

/**
 * @author: zhanghe
 * @date: 2020/5/11 22:39
 */
public class InterfImpl implements Interf {
    @Override
    public String f(String msg) {
        return msg + " 普通实现";
    }

    static String func(String msg) {
        return msg + " 方法引用";
    }

    public static void main(String[] args) {
        Interf[] interfs = new Interf[]{
                new InterfImpl(),//普通实现
                new Interf() {
                    @Override
                    public String f(String msg) {
                        return msg + " 匿名内部类";
                    }
                },
                msg -> msg + " lambda",
                InterfImpl::func
        };

        for(Interf interf : interfs){
            System.out.println(interf.f("测试lambda"));
        }
    }
}
