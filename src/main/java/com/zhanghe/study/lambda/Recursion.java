package com.zhanghe.study.lambda;

/**
 * @author: zhanghe
 * @date: 2020/5/11 23:49
 */
public class Recursion {

    private IntCall intCall;

    public static void main(String[] args) {
        Recursion recursion = new Recursion();
        recursion.intCall = n -> n == 0 ? 1 : n * recursion.intCall.call(n - 1);
        for (int i = 0; i < 10; i++)
            System.out.println(i+"! = "+recursion.intCall.call(i));
    }
}
