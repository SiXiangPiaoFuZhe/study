package com.zhanghe.study.lambda;

/**
 * @author: zhanghe
 * @date: 2020/5/12 0:26
 */
public class MethodReferences {
    public static void main(String[] args) {
        Describe describe = new Describe();
        Callable callable = describe :: show;
        callable.call("测试方法引用");
    }
}

class Describe {
    void show(String msg) {
        System.out.println(msg);
    }
}
