package com.zhanghe.study.lambda;

/**
 * @author: zhanghe
 * @date: 2020/5/11 23:47
 */
@FunctionalInterface
public interface IntCall {
    int call(int i);
}
