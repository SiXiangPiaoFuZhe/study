package com.zhanghe.study.lambda;

/**
 * @author: zhanghe
 * @date: 2020/5/11 22:39
 */
@FunctionalInterface
public interface Interf {
    String f(String msg);
}
