package com.zhanghe.study.kilim;

import kilim.ExitMsg;
import kilim.Mailbox;
import kilim.Task;

/**
 * @author zh
 * @date 2022/4/20 11:03
 */
public class HelloWorldTask extends Task {

    static Mailbox<String> mailbox = new Mailbox<>();
    static Mailbox<ExitMsg> exitMsgMailbox = new Mailbox<>();
    // 0表示接收者，1表示发送者
    int type = 0;

    public HelloWorldTask(int type){
        this.type = type;
    }

    @Override
    public void execute() throws Exception {
        if(type == 0){ // 接收者
            while (true){
                String s = mailbox.get();
                if("over".equals(s)){
                    break;
                }
                System.out.println(s);
            }
        } else if(type == 1){ // 发送者，向邮箱发送信息
            mailbox.putnb("Hello");
            mailbox.putnb("World");
            mailbox.putnb("over");
        }

    }

    public static void main(String[] args) {
        Task sender = new HelloWorldTask(1).start();
        Task reciever = new HelloWorldTask(0).start();

        reciever.informOnExit(exitMsgMailbox); // 接收者结束后，填写退出邮箱
        exitMsgMailbox.getb();
        System.exit(0);
    }

}
