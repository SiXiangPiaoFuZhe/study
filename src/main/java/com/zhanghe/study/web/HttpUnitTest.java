package com.zhanghe.study.web;


import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.List;

/**
 * @author zh
 * @date 2023/10/11 15:23
 */
public class HttpUnitTest {
    public static void main(String[] args) {
        try (WebClient webClient = new WebClient()) {
            //新建一个模拟谷歌Chrome浏览器的浏览器客户端对象

            webClient.getOptions().setThrowExceptionOnScriptError(true);//当JS执行出错的时候是否抛出异常, 这里选择不需要
            webClient.getOptions().setThrowExceptionOnFailingStatusCode(true);//当HTTP的状态非200时是否抛出异常, 这里选择不需要
            webClient.getOptions().setActiveXNative(true);
            webClient.getOptions().setCssEnabled(false);//是否启用CSS, 因为不需要展现页面, 所以不需要启用
            webClient.getOptions().setJavaScriptEnabled(true); //很重要，启用JS
//忽略css错误
            webClient.setCssErrorHandler(new SilentCssErrorHandler());
            webClient.setAjaxController(new NicelyResynchronizingAjaxController());//很重要，设置支持AJAX
//超时时间
            webClient.getOptions().setTimeout(3000);
            //允许重定向
            webClient.getOptions().setRedirectEnabled(true);
            //允许cookie
            webClient.getCookieManager().setCookiesEnabled(true);
            HtmlPage page = webClient.getPage("https://nlc.chinanutri.cn/fq/");//尝试加载上面图片例子给出的网页

//            webClient.waitForBackgroundJavaScript(3000);
            String pageXml = page.asXml();//直接将加载完成的页面转换成xml格式的字符串

            Document document = Jsoup.parse(pageXml);//获取html文档
            Elements indexContent = document.getElementsByClass("index_content");
            if (indexContent != null && indexContent.size() > 0) {
                Element element = indexContent.get(0);
                Elements children = element.children();
                if(children != null && children.size() > 3){
                    for(int i = 3;i<children.size();i++){
                        // 食物盒子
                        Element foodBox = children.get(i);
                        // div h3 a
                        // <a href="foodlist_0_1_0_0_0_1.htm" data_id="1" data_pid="0" target="_blank">谷类及制品</a>
                        Element category = foodBox.child(0).child(0).child(0);
                        String categoryId = category.attr("data_id");
                        String categoryName = category.text();
                        // ul
                        Element footGroupList = foodBox.child(1);
                        Elements foodGroupListElements = footGroupList.children();
                        for(Element foodGroupElement : foodGroupListElements){
                            // a
                            // <a href="foodlist_0_1_30_0_0_1.htm" data_id="30" data_pid="1" target="_blank">小麦</a>
                            Element food = foodGroupElement.child(0);
                            String foodId = food.attr("data_id");
                            String foodName = food.text();

                            System.out.println("foodId:"+foodId+"--foodName:"+foodName);


                        }

                        System.out.println("");

                    }
                }


            }

//            System.out.println(pageXml);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static class FoodCategory{
        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class FoodGroup{
        private int id;
        private String name;
        private int category_id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getCategory_id() {
            return category_id;
        }

        public void setCategory_id(int category_id) {
            this.category_id = category_id;
        }
    }



    public static class Food{

        private int id;
        private int group_id;
        private String name;
        private String alias_name;
        private String en_name;
        // -----能量与相关成分-----
        // 食部
        private String edible;
        // 水分
        private String water;
        // 能量
        private String energy;
        // 蛋白质
        private String protein;
        // 脂肪
        private String fat;
        // 胆固醇
        private String cholesterol;
        // 灰分
        private String ash;
        // 碳水化合物
        private String cho;
        // 总膳食纤维
        private String dietary_fiber;
        // -------维生素------
        // 胡萝卜素
        private String carotene;
        // 维生素A
        private String vitamin;
        // α-TE
        private String te;
        // 硫胺素
        private String thiamin;
        // 核黄素
        private String riboflavin;
        // 烟酸
        private String niacin;
        // 维生素C
        private String vitamin_c;
        // -------矿物质
        // 钙
        private String ca;
        // 磷
        private String p;
        // 钾
        private String k;
        // 钠
        private String na;
        // 镁
        private String mg;
        // 铁
        private String fe;
        // 锌
        private String zn;
        // 硒
        private String se;
        // 铜
        private String cu;
        // 锰
        private String mn;
        // 碘
        private String i;
        // -------脂肪酸
        // 饱和脂肪酸
        private String sfa;
        // 单不饱和脂肪酸
        private String mufa;
        // 多不饱和脂肪酸
        private String pufa;
        // 合计
        private String total;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAlias_name() {
            return alias_name;
        }

        public void setAlias_name(String alias_name) {
            this.alias_name = alias_name;
        }

        public String getEn_name() {
            return en_name;
        }

        public void setEn_name(String en_name) {
            this.en_name = en_name;
        }

        public String getEdible() {
            return edible;
        }

        public void setEdible(String edible) {
            this.edible = edible;
        }

        public String getWater() {
            return water;
        }

        public void setWater(String water) {
            this.water = water;
        }

        public String getEnergy() {
            return energy;
        }

        public void setEnergy(String energy) {
            this.energy = energy;
        }

        public String getProtein() {
            return protein;
        }

        public void setProtein(String protein) {
            this.protein = protein;
        }

        public String getFat() {
            return fat;
        }

        public void setFat(String fat) {
            this.fat = fat;
        }

        public String getCholesterol() {
            return cholesterol;
        }

        public void setCholesterol(String cholesterol) {
            this.cholesterol = cholesterol;
        }

        public String getAsh() {
            return ash;
        }

        public void setAsh(String ash) {
            this.ash = ash;
        }

        public String getCho() {
            return cho;
        }

        public void setCho(String cho) {
            this.cho = cho;
        }

        public String getDietary_fiber() {
            return dietary_fiber;
        }

        public void setDietary_fiber(String dietary_fiber) {
            this.dietary_fiber = dietary_fiber;
        }

        public String getCarotene() {
            return carotene;
        }

        public void setCarotene(String carotene) {
            this.carotene = carotene;
        }

        public String getVitamin() {
            return vitamin;
        }

        public void setVitamin(String vitamin) {
            this.vitamin = vitamin;
        }

        public String getTe() {
            return te;
        }

        public void setTe(String te) {
            this.te = te;
        }

        public String getThiamin() {
            return thiamin;
        }

        public void setThiamin(String thiamin) {
            this.thiamin = thiamin;
        }

        public String getRiboflavin() {
            return riboflavin;
        }

        public void setRiboflavin(String riboflavin) {
            this.riboflavin = riboflavin;
        }

        public String getNiacin() {
            return niacin;
        }

        public void setNiacin(String niacin) {
            this.niacin = niacin;
        }

        public String getVitamin_c() {
            return vitamin_c;
        }

        public void setVitamin_c(String vitamin_c) {
            this.vitamin_c = vitamin_c;
        }

        public String getCa() {
            return ca;
        }

        public void setCa(String ca) {
            this.ca = ca;
        }

        public String getP() {
            return p;
        }

        public void setP(String p) {
            this.p = p;
        }

        public String getK() {
            return k;
        }

        public void setK(String k) {
            this.k = k;
        }

        public String getNa() {
            return na;
        }

        public void setNa(String na) {
            this.na = na;
        }

        public String getMg() {
            return mg;
        }

        public void setMg(String mg) {
            this.mg = mg;
        }

        public String getFe() {
            return fe;
        }

        public void setFe(String fe) {
            this.fe = fe;
        }

        public String getZn() {
            return zn;
        }

        public void setZn(String zn) {
            this.zn = zn;
        }

        public String getSe() {
            return se;
        }

        public void setSe(String se) {
            this.se = se;
        }

        public String getCu() {
            return cu;
        }

        public void setCu(String cu) {
            this.cu = cu;
        }

        public String getMn() {
            return mn;
        }

        public void setMn(String mn) {
            this.mn = mn;
        }

        public String getI() {
            return i;
        }

        public void setI(String i) {
            this.i = i;
        }

        public String getSfa() {
            return sfa;
        }

        public void setSfa(String sfa) {
            this.sfa = sfa;
        }

        public String getMufa() {
            return mufa;
        }

        public void setMufa(String mufa) {
            this.mufa = mufa;
        }

        public String getPufa() {
            return pufa;
        }

        public void setPufa(String pufa) {
            this.pufa = pufa;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getGroup_id() {
            return group_id;
        }

        public void setGroup_id(int group_id) {
            this.group_id = group_id;
        }
    }


    public static class Response{
        private int currentPage;
        private int totalPages;
        private List<List> list;

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public List<List> getList() {
            return list;
        }

        public void setList(List<List> list) {
            this.list = list;
        }
    }
}
