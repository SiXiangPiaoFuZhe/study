package com.zhanghe.study;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author zh
 * @date 2024/2/19 10:34
 */
public class Main {
    public static void main(String[] args) {
        LocalDate localDate = LocalDate.of(2022,9,30);
        LocalDate today = LocalDate.of(2022,10,17);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");

        for(LocalDate cur = localDate;cur.isBefore(today);cur = cur.plusDays(1)){
            System.out.println("sh tmysql_v2.sh "+cur.format(formatter) +" DMI_KYLIN_TRACK_PV_I_D");
        }
    }
}
