package com.zhanghe.study.suanfa;

/**
 * 回溯算法  八皇后
 *
 * @author: zhanghe
 * @date: 2020/5/30 23:01
 */
public class Main5 {
    public static int num = 0; // 方案数
    public static final int MAXQUEEN = 8; // 皇后数
    public static int[] cols = new int[MAXQUEEN]; // 定义数组，表示MAXQUEEN列棋子中皇后摆放位置

    public void mm(int n){

    }

    /*
     * @param n:填第n列的皇后
     */

    public void getCount(int n) {
        boolean[] rows = new boolean[MAXQUEEN];
        for (int m = 0; m < n; m++) {
            rows[cols[m]] = true;
            int d = n - m;
            // 正斜方向
            if (cols[m] - d >= 0) {
                rows[cols[m] - d] = true;
            }
            // 反斜方向
            if (cols[m] + d <= (MAXQUEEN - 1)) {
                rows[cols[m] + d] = true;
            }
        }
        for (int i = 0; i < MAXQUEEN; i++) {
            if (rows[i]) {
                // 不能放
                continue;
            }
            cols[n] = i;
            // 下面仍然有合法位置
            if (n < MAXQUEEN - 1) {
                getCount(n + 1);
            } else {
                // 找到完整的一套方案
                num++;
                printQueen();
            }
        }
    }

    private void printQueen() {
        System.out.println("第" + num + "种方案");
        for (int i = 0; i < MAXQUEEN; i++) {
            for (int j = 0; j < MAXQUEEN; j++) {
                if (i == cols[j]) {
                    System.out.print("0 ");
                } else {
                    System.out.print("+ ");
                }
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Main5 queen = new Main5();
        queen.getCount(0);
    }
}
