package com.zhanghe.study.suanfa;

/**
 * 求最大公约数  欧几里得算法
 *
 * @author: zhanghe
 * @date: 2020/6/3 14:09
 */
public class Main7 {
    public static void main(String[] args) {

        System.out.println(new Main7().maxCommonDisvisor(48,36));
    }

    public int maxCommonDisvisor(int m, int n) {
        int temp = 0;
        if (m < n) {
            while (true) {
                if (n % m == 0) {
                    return m;
                }
                temp = n % m;
                n = m;
                m = temp;
            }
        } else {
            while (true) {
                if (m % n == 0) {
                    return n;
                }
                temp = m % n;
                m = n;
                n = temp;
            }
        }
    }
}
