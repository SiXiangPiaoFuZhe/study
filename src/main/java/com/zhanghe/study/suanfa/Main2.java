package com.zhanghe.study.suanfa;

import java.util.Arrays;

/**
 * 斐波那契数列
 * @author: zhanghe
 * @date: 2020/5/28 9:46
 */
public class Main2 {
    public static void main(String[] args) {
        Main2 main2 = new Main2();
        main2.fs(10);
    }

    public void fs(int num) {
        int[] fs = new int[num];
        fs[0] = 1;
        fs[1] = 1;
        for (int i = 2; i < num; i++) {
            fs[i] = fs[i - 1] + fs[i - 2];
        }
        Arrays.stream(fs).forEach(System.out :: println);
    }
}
