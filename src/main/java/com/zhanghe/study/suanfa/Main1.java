package com.zhanghe.study.suanfa;

/**
 * 100~200之间素数
 *
 * @author: zhanghe
 * @date: 2020/5/26 20:16
 */
public class Main1 {
    public static void main(String[] args) {
        // 由于偶数可以被2整除  肯定不是素数
        for (int i = 101; i < 200; i = i + 2) {
            boolean isSuShu = true;
            for (int j = 2; j <= Math.sqrt(i); j++) {
                if (i % j == 0) {
                    isSuShu = false;
                    break;
                }
            }
            if (isSuShu) {
                System.out.printf("%d  ", i);
            }
        }
    }
}
