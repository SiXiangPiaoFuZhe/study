package com.zhanghe.study.suanfa;

/**
 * 进制转换  递归
 * @author: zhanghe
 * @date: 2020/5/28 10:02
 */
public class Main3 {
    public static void main(String[] args) {
        System.out.println(Integer.toBinaryString(10));
        StringBuilder stringBuilder = new StringBuilder();
        tenToTwo(10, stringBuilder);
        System.out.println(stringBuilder.reverse().toString());
    }

    public static int tenToTwo(int ten, StringBuilder sb) {
        if (ten == 0) {
            return 0;
        }
        sb.append(ten % 2);
        return tenToTwo(ten / 2, sb);
    }
}
