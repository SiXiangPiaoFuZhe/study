package com.zhanghe.study.io.old;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 数据流  用来处理基本数据类型、String、字节数组
 * DataInputStream
 * DataOutputStream
 */
public class TestDataStream {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        DataOutputStream dos = null;
        try {
            fos = new FileOutputStream("data.txt");
            dos = new DataOutputStream(fos);

            dos.writeUTF("测试数据流");
            dos.writeLong(100L);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(dos != null){
                try {
                    dos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if(fos != null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
