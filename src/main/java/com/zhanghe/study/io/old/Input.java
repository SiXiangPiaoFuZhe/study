package com.zhanghe.study.io.old;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author: zhanghe
 * @date: 2020/6/16 9:37
 */
public class Input {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.equals("end")) {
                    break;
                }
                System.out.println(line);
            }
        } catch (IOException e) {

        }
    }
}
