package com.zhanghe.study.io.old;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * 打印流
 * PrintStream  输出字节流
 * PrintWriter  输出字符流
 */
public class TestPrintStream {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File("log.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // 创建打印输出流  true为自动刷新(写入换行符或字节\n会自动刷新缓冲区)
        PrintStream ps = new PrintStream(fos,true);
        if(ps != null){
            // 重现设置输出流  打印到文件中
            System.setOut(ps);
        }
        System.out.println("测试一下");
        ps.close();

    }
}
