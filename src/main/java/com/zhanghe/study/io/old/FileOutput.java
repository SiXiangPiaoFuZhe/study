package com.zhanghe.study.io.old;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author: zhanghe
 * @date: 2020/6/11 10:48
 */
public class FileOutput {
    public static void main(String[] args) throws IOException {
//        try(BufferedReader in = new BufferedReader(new StringReader("1111111111111111\n2222222222222\n3333333333"));
//            PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter("test.txt")))
//        ){
//            in.lines().forEach(printWriter :: println);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        List<String> collect = Files.lines(Paths.get("/Users/zhanghe/Downloads/109449.result.csv"))
                .collect(Collectors.toList());

        Map<String,Integer> count = new HashMap<>();
        for(String s : collect){
            if(count.containsKey(s)){
                count.put(s,count.get(s)+1);
            } else {
                count.put(s,1);
            }
        }

        int threeCount = 0;
        for(Map.Entry<String,Integer> entry : count.entrySet()){
            if(entry.getValue() >= 1){
                threeCount++;
            }
        }

        System.out.println(threeCount);


    }
}
