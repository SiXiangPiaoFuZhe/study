package com.zhanghe.study.io.old;

import java.io.IOException;
import java.io.StringReader;

/**
 * @author: zhanghe
 * @date: 2020/6/11 10:31
 */
public class StringReaderTest {
    public static void main(String[] args) throws IOException {
        read();
    }

    public static void read() throws IOException {
        StringReader stringReader = new StringReader("qaw试试");
        int c;
        while ((c = stringReader.read()) != -1) {
            System.out.println((char)c);
        }
    }
}
