package com.zhanghe.study.io.old;

import java.io.*;

/**
 * @author: zhanghe
 * @date: 2020/6/16 10:22
 */
public class Redirect {
    public static void main(String[] args) {
        try(BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream("C:\\Users\\sinosoft\\Desktop\\剩余工作.txt"));
            PrintStream printStream = new PrintStream(new BufferedOutputStream(new FileOutputStream("C:\\Users\\sinosoft\\Desktop\\剩余工作副本.txt")))
        ){
            System.setIn(bufferedInputStream);
            System.setOut(printStream);
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    if (line.equals("end")) {
                        break;
                    }
                    System.out.println(line);
                }
            } catch (IOException e1) {

            }
        }catch (IOException e){

        }
    }
}
