package com.zhanghe.study.io.old;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author: zhanghe
 * @date: 2020/6/10 20:16
 */
public class BufferInputFile {
    public static void main(String[] args) throws FileNotFoundException {
        read("BufferInputFile.java");
    }

    public static void read(String file) {
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(file))){
            String line = null;
            while((line = bufferedReader.readLine()) != null){
                System.out.println(line);
            }
        } catch (IOException e) {
            throw new RuntimeException("读取失败",e);
        }
    }
}
