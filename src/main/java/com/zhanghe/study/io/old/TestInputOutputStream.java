package com.zhanghe.study.io.old;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 输入输出是站在程序的角度来分的
 * 输入：读取外部数据道程序
 * 输出：将程序数据输出
 *
 *
 */
public class TestInputOutputStream {

    public static void main(String[] args)  {
        System.out.println("--------testInputStreamRead()-------------");
        testInputStreamRead();
        System.out.println();
        System.out.println("--------testInputStreamRead(int length)-------------");
        testInputStreamRead(10);
        System.out.println("--------testOutputStreamWrite()-------------");
        testOutputStreamWrite();
        System.out.println("--------copyFile(String src,String target)-------------");
        copyFile("hello2.txt","hello.txt");
    }

    /**
     * 测试inputStream的read方法
     */
    private static void testInputStreamRead(){
        FileInputStream fis = null;
        try{
            // 创建一个文件
            File file = new File("hello.txt");
            // 创建一个输入流
            fis = new FileInputStream(file);
            int b;
            // read() 会将下一个字节读取出来，如果没有了，就返回-1
            while ((b = fis.read()) != -1){
                System.out.print((char)b);
            }
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            try {
                if(fis != null){
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 测试inputStream的read方法
     * @param length 字节数组长度
     */
    private static void testInputStreamRead(int length){
        FileInputStream fis = null;
        try{
            File file = new File("hello.txt");
            fis = new FileInputStream(file);
            // 读取到数据存入到数组中
            byte[] b = new byte[length];
            // 每次写入到数组中的字节长度
            int len;
            while ((len = fis.read(b)) != -1 ){
                for(int i=0;i<len;i++){
                    System.out.print((char)b[i]);
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            try {
                if(fis != null){
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * 测试OutputStream的write方法
     */
    private static void testOutputStreamWrite(){
        FileOutputStream fos = null;
        try{
            File file = new File("hello2.txt");
            fos = new FileOutputStream(file);
            fos.write("I love you".getBytes());
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            try {
                if(fos != null){
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 复制文件
     * @param src 源文件
     * @param target 目标文件
     */
    private static void copyFile(String src,String target){
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try{
            File srcFile = new File(src);
            File targetFile = new File(target);
            fis = new FileInputStream(srcFile);
            fos = new FileOutputStream(targetFile);
            byte[] b = new byte[10];
            int len;
            while ((len = fis.read(b)) != -1){
                // 要使用len  不可使用fos.write(b) 因为fos.write(b)中使用的是writeBytes(b, 0, b.length, append);
                // b.length会导致最后一次传输的数据不准确
                fos.write(b,0,len);
            }
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            if(fos != null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if(fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
