package com.zhanghe.study.io.old;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * 标准流
 * 标准输出流 System.out
 * 标准输入流 System.in
 */
public class TestSystemStream {
    public static void main(String[] args) {
        InputStream is = System.in;
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader reader = new BufferedReader(isr);
        System.out.println("请输入字符串");
        String str = "";
        while (true){
            try {
                str = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if("exit".equals(str)){
                break;
            }
            System.out.println(str);
        }
    }
}
