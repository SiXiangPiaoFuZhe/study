package com.zhanghe.study.io.nio;

import org.junit.Test;

import java.nio.ByteBuffer;

/**
 * @author zh
 * @date 2021/8/17 10:24
 */
public class TestBuffer {

    @Test
    public void test(){
        // 分配一个指定大小的缓冲区
        ByteBuffer buf = ByteBuffer.allocate(1024);
        // 0
        System.out.println(buf.position());
        // 1024
        System.out.println(buf.limit());
        // 1024
        System.out.println(buf.capacity());

        buf.put("hello".getBytes());
        System.out.println("写操作后");
        // 5
        System.out.println(buf.position());
        // 1024
        System.out.println(buf.limit());
        // 1024
        System.out.println(buf.capacity());

        buf.flip();
        System.out.println("切换为读模式");
        // 0
        System.out.println(buf.position());
        // 5
        System.out.println(buf.limit());
        // 1024
        System.out.println(buf.capacity());

        byte[] read = new byte[buf.limit()];
        buf.get(read);
        System.out.println(new String(read,0,read.length));

        System.out.println("数据读取之后");
        // 5
        System.out.println(buf.position());
        // 5
        System.out.println(buf.limit());
        // 1024
        System.out.println(buf.capacity());


        buf.rewind();
        System.out.println("重新读取");
        // 0
        System.out.println(buf.position());
        // 5
        System.out.println(buf.limit());
        // 1024
        System.out.println(buf.capacity());

        byte[] two = new byte[2];
        buf.get(two);
        System.out.println(new String(two,0,two.length));
        System.out.println("读取2个字节");
        // 2
        System.out.println(buf.position());
        // 5
        System.out.println(buf.limit());
        // 1024
        System.out.println(buf.capacity());
        // 标记当前位置
        buf.mark();
        buf.get(two);
        System.out.println(new String(two,0,two.length));
        System.out.println("再次读取2个字节");
        // 4
        System.out.println(buf.position());
        // 5
        System.out.println(buf.limit());
        // 1024
        System.out.println(buf.capacity());

        buf.reset();
        System.out.println("重置");
        // 2
        System.out.println(buf.position());
        // 5
        System.out.println(buf.limit());
        // 1024
        System.out.println(buf.capacity());

        buf.clear();
        System.out.println("清空缓冲区");
        // 0
        System.out.println(buf.position());
        // 1024
        System.out.println(buf.limit());
        // 1024
        System.out.println(buf.capacity());

        byte[] clearData = new byte[5];
        buf.get(clearData);
        // 清空缓冲区之后数据还在，只是position和limit恢复到了原本的状态
        // hello
        System.out.println(new String(clearData,0,clearData.length));

    }

    public void test1(){
        // 非直接缓冲区
        ByteBuffer allocate = ByteBuffer.allocate(10);
        // 直接缓冲区
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(10);
    }
}
