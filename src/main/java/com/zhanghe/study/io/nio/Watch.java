package com.zhanghe.study.io.nio;

import java.io.IOException;
import java.nio.file.*;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;

/**
 * @author zh
 * @date 2025/1/9 10:37
 */
public class Watch {
    public static void main(String[] args) throws IOException, InterruptedException {
        // 监听器
        WatchService watchService = FileSystems.getDefault().newWatchService();

        Path path = Paths.get("/Users/zhanghe/Desktop");
        // 监听创建事件
        path.register(watchService,ENTRY_CREATE);
        // 会进行阻塞
        WatchKey take = watchService.take();

        for(WatchEvent event : take.pollEvents()){
            System.out.println("evt.context(): " + event.context() +
                    " evt.count(): " + event.count() +
                    " evt.kind(): " + event.kind());
        }
    }
}
