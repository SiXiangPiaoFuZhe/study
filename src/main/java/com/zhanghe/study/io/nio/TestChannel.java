package com.zhanghe.study.io.nio;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @author zh
 * @date 2021/8/17 11:08
 */
public class TestChannel {

    public static void main(String[] args) throws IOException {
        RandomAccessFile tdat = new RandomAccessFile("test.dat", "rw");
        MappedByteBuffer out = tdat.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, 100);

    }
}
