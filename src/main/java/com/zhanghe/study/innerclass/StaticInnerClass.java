package com.zhanghe.study.innerclass;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class StaticInnerClass {

    private static final String PASSWORD_CRYPT_KEY = "trendyworkToken";

    private final static String AES = "AES";

    /*
     * 加密
     * 1.构造密钥生成器
     * 2.根据ecnodeRules规则初始化密钥生成器
     * 3.产生密钥
     * 4.创建和初始化密码器
     * 5.内容加密
     * 6.返回字符串
     */
    public static String aesEncode(String content){
        try {
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");//修改后
            random.setSeed(PASSWORD_CRYPT_KEY.getBytes());
            //1.构造密钥生成器，指定为AES算法,不区分大小写
            KeyGenerator keygen=KeyGenerator.getInstance(AES);
            //2.根据ecnodeRules规则初始化密钥生成器
            //生成一个128位的随机源,根据传入的字节数组
            keygen.init(128, random);
            //3.产生原始对称密钥
            SecretKey original_key=keygen.generateKey();
            //4.获得原始对称密钥的字节数组
            byte [] raw=original_key.getEncoded();
            //5.根据字节数组生成AES密钥
            SecretKey key=new SecretKeySpec(raw, AES);
            //6.根据指定算法AES自成密码器
            Cipher cipher=Cipher.getInstance(AES);
            //7.初始化密码器，第一个参数为加密(Encrypt_mode)或者解密解密(Decrypt_mode)操作，第二个参数为使用的KEY
            cipher.init(Cipher.ENCRYPT_MODE, key);
            //8.获取加密内容的字节数组(这里要设置为utf-8)不然内容中如果有中文和英文混合中文就会解密为乱码
            byte [] byte_encode=content.getBytes(StandardCharsets.UTF_8);
            //9.根据密码器的初始化方式--加密：将数据加密
            byte [] byte_AES=cipher.doFinal(byte_encode);
            //10.将加密后的数据转换为字符串
            //这里用Base64Encoder中会找不到包
            //解决办法：
            //在项目的Build path中先移除JRE System Library，再添加库JRE System Library，重新编译后就一切正常了。
            //11.将字符串返回
            return new BASE64Encoder().encode(byte_AES);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }

        //如果有错就返加nulll
        return null;
    }
    /*
     * 解密
     * 解密过程：
     * 1.同加密1-4步
     * 2.将加密后的字符串反纺成byte[]数组
     * 3.将加密内容解密
     */
    public static String aesDncode(String content){
        try {

            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");//修改后
            random.setSeed(PASSWORD_CRYPT_KEY.getBytes());
            //1.构造密钥生成器，指定为AES算法,不区分大小写
            KeyGenerator keygen=KeyGenerator.getInstance(AES);
            //2.根据ecnodeRules规则初始化密钥生成器
            //生成一个128位的随机源,根据传入的字节数组
            keygen.init(128, random);
            //3.产生原始对称密钥
            SecretKey original_key=keygen.generateKey();
            //4.获得原始对称密钥的字节数组
            byte [] raw=original_key.getEncoded();
            //5.根据字节数组生成AES密钥
            SecretKey key=new SecretKeySpec(raw, AES);
            //6.根据指定算法AES自成密码器
            Cipher cipher=Cipher.getInstance(AES);
            //7.初始化密码器，第一个参数为加密(Encrypt_mode)或者解密(Decrypt_mode)操作，第二个参数为使用的KEY
            cipher.init(Cipher.DECRYPT_MODE, key);
            //8.将加密并编码后的内容解码成字节数组
            byte [] byte_content= new BASE64Decoder().decodeBuffer(content);
            /*
             * 解密
             */
//            byte[] resBytes = content.getBytes(StandardCharsets.UTF_8);
            byte [] byte_decode=cipher.doFinal(byte_content);
            return new String(byte_decode, StandardCharsets.UTF_8);
        } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | IOException e) {
            e.printStackTrace();
        }

        //如果有错就返加nulll
        return null;
    }

    public static void main(String[] args) {
        String info = "|  1 | trendy                       | 7f4176a74bce911a372e292388a180ac |," +
                "|  2 | bjwzb                        | d5c74c3f4577e89d4cd70f1042b92f4f |," +
                "|  6 | tong.hua@trendimedia.com     | 9c6162bbf3f64ae32191dbb53d3bbd0d |," +
                "| 22 | michelle.ma@trendimedia.com  | 7f4176a74bce911a372e292388a180ac |," +
                "| 23 | ming.xuan@trendimedia.com    | 7f4176a74bce911a372e292388a180ac |," +
                "| 24 | hong.wei@trendimedia.com     | 3ca383f2f4cbcee28053ac90ea1955bf |," +
                "| 25 | loreal                       | 50dd2a695cef02918e7effc8b68ac53f |," +
                "| 26 | steven.zhou@trendimedia.com  | 928a4e92571a04ec02e69a81d97bf10c |," +
                "| 27 | yvonne.cheng@trendimedia.com | dd08a0480f3a86d10adf309929e60f4d |," +
                "| 28 | karser.peng@trendimedia.com  | 928a4e92571a04ec02e69a81d97bf10c |," +
                "| 29 | qianyun.peng@trendimedia.com | 7f4176a74bce911a372e292388a180ac |," +
                "| 30 | frank.wang@trendimedia.com   | 15a6909df65e1b85e3c2e76ad1f12274 |," +
                "| 31 | jonathan.li@trendimedia.com  | 7b7e2acd76e7d0828da7a03a2aea183c |," +
                "| 32 | rita.wei@trendimedia.com     | d774cea24c4ef4bd0b604667e64e2711 |," +
                "| 33 | minmin.zhou@trendimedia.com  | 1aee7793a9f3f5dc68cd3c2b890acada |," +
                "| 34 | minhui.xia@trendimedia.com   | bcde1148dcb42450efd938bd5d69b31a |," +
                "| 35 | andy.ma@trendimedia.com      | 928a4e92571a04ec02e69a81d97bf10c |," +
                "| 36 | wenjie.xu@trendimedia.com    | add5c80d4e62f3547ee7efeb97ae9aad |," +
                "| 37 | hopkins.he@trendimedia.com   | 8f276e4511162805a0e617ee62ac679c |," +
                "| 38 | testLocal                    | 31942e3e8e35a55e03ed2063ba191077 |," +
                "| 39 | jane.cheng@trendimedia.com   | a57f98f9dc137abd0d3d8f466e79178f |," +
                "| 40 | kumiko.liu@trendimedia.com   | 53f117fc6810ed0ed739843b198e3fc8 |," +
                "| 41 | oliver.yin@trendimedia.com   | 7f4176a74bce911a372e292388a180ac |," +
                "| 42 | yi.luo@trendimedia.com       | 7f4176a74bce911a372e292388a180ac |," +
                "| 43 | qingzi.wang@trendimedia.com  | 7f4176a74bce911a372e292388a180ac |," +
                "| 44 | adidas@trendimedia.com       | 7f4176a74bce911a372e292388a180ac |," +
                "| 45 | nancy.huang@trendimedia.com  | e5eda65a683c357636ea8664b4495805 |," +
                "| 46 | zybang@trendimedia.com       | b46928508843c4e0e2cc9c510915037b |," +
                "| 47 | shaohua.lu@trendimedia.com   | 1a809408dfca179078fafb75c4bd3a7e |," +
                "| 48 | sunny.shen@trendimedia.com   | 900870f23ac0e027f86872abb5da1717 |," +
                "| 49 | chelsey.wei@trendimedia.com  | d476a16a1ce6e38714a576f754df6273 |," +
                "| 50 | may.liu@trendimedia.com      | 85a5ffc70f1654b547c86ffa408adfb8 |," +
                "| 51 | test@trendimedia.com         | 7f4176a74bce911a372e292388a180ac |," +
                "| 52 | trendibuzz@trendimedia.com   | 928a4e92571a04ec02e69a81d97bf10c |," +
                "| 53 | trenditest                   | b0bccba3a1f2b85999cca2df22383228 |," +
                "| 54 | demo                         | 62cc2d8b4bf2d8728120d052163a77df |," +
                "| 55 | loreal@trendimedia.com       | 5cd404afb049c07b4e8a907838a2d18a |," +
                "| 56 | meng.li@trendimedia.com      | 7f4176a74bce911a372e292388a180ac |," +
                "| 57 | 1107529331@qq.com            | 7f4176a74bce911a372e292388a180ac |," +
                "| 58 | he.zhang@trendimedia.com     | 050f5b2fdc60891c76af619d91078a93 |," +
                "| 59 | test                         | e4f02d45e37e738643c49c57497f4d5a |";
        String[] infos = info.split(",");
        for(String user : infos){
            String[] userInfo = user.split("\\|");
            int id = Integer.parseInt(userInfo[1].trim());
            String name = userInfo[2].trim();
            String psw = userInfo[3].trim();
            String sql = String.format("update admin set user_name= '%s',password = '%s' where id = %d;",aesEncode(name),aesEncode(psw),id);
            System.out.println(sql);
        }
    }
}
