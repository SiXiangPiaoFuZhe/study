package com.zhanghe.study.innerclass;

public class NonStaticInnerClass {
    public static void main(String[] args) throws ClassNotFoundException {
        NonStaticInnerClass nonStaticInnerClass = new NonStaticInnerClass();
        NonStaticInnerClass.Inner inner = nonStaticInnerClass.new Inner();


        System.out.println(Inner.class.getName());

        Class<?> aClass = Class.forName("com.zhanghe.study.innerclass.NonStaticInnerClass$Inner");

        System.out.println(Inner2.class.getName());
    }

    class Inner{

    }
}

class Inner2{

}
