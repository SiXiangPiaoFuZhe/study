package com.zhanghe.study.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;

/**
 * 远程端口 提供客户端需要的服务  需要继承接口Remote
 * @author zh
 * @date 2020/12/16 12:09
 */
public interface RemoteServer extends Remote {

    Date getDate() throws RemoteException;
}
