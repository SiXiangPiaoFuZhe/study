package com.zhanghe.study.rmi;

/**
 * @author zh
 * @date 2020/12/16 12:11
 */
public abstract class WorkRequest extends Request{
    public abstract Object task();
}
