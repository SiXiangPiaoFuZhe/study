package com.zhanghe.study.rmi;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;

/**
 * 远程接口实现类
 * 编译注册完之后  执行rmic  全路径生成stub
 * @author zh
 * @date 2020/12/16 12:14
 */
public class MyServer extends UnicastRemoteObject implements RemoteServer {

    protected MyServer() throws RemoteException {
        super();
    }


    @Override
    public Date getDate() throws RemoteException {
        return new Date();
    }

    public Object task(WorkRequest request) throws RemoteException{
        return request.task();
    }

    public static void main(String[] args) {
        try {
            // 注册
            RemoteServer server = new MyServer();
            Registry registry = LocateRegistry.createRegistry(1100);
            registry.rebind("//127.0.0.1:1100/MyServer",server);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
