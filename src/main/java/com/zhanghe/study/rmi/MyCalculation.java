package com.zhanghe.study.rmi;

/**
 * @author zh
 * @date 2020/12/16 12:12
 */
public class MyCalculation extends WorkRequest{

    private int m;
    private int n;
    public MyCalculation(int m,int n){
        this.m = m;
        this.n = n;
    }
    @Override
    public Object task() {
        return m + n;
    }
}
