package com.zhanghe.study.event;

import java.util.EventObject;

/**
 * 事件 继承EventObject
 * @author zh
 * @date 2024/1/30 10:58
 */
public class CusEvent extends EventObject {
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public CusEvent(Object source) {
        super(source);
    }
}
