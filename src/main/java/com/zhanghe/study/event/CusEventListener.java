package com.zhanghe.study.event;

import java.util.EventListener;

/**
 * 事件监听 实现EventListener，定义回调接口
 *
 * @author zh
 * @date 2024/1/30 10:59
 */
public class CusEventListener implements EventListener {

    public void fire(CusEvent event) {
        Object source = event.getSource();
        System.out.println("接收事件源" + source);
    }

}
